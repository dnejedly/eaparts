<?php
/**
 * MageParts
 * 
 * NOTICE OF LICENSE
 * 
 * This code is copyrighted by MageParts and may not be reproduced
 * and/or redistributed without a written permission by the copyright 
 * owners. If you wish to modify and/or redistribute this file please
 * contact us at info@mageparts.com for confirmation before doing
 * so. Please note that you are free to modify this file for personal
 * use only.
 *
 * If you wish to make modifications to this file we advice you to use
 * the "local" file scope in order to aviod conflicts with future updates. 
 * For information regarding modifications see http://www.magentocommerce.com.
 *  
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF 
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY 
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * @category   MageParts
 * @package    MageParts_MediaLibrary
 * @copyright  Copyright (c) 2009 MageParts (http://www.mageparts.com/)
 * @author 	   MageParts Crew
 */

class MageParts_MediaLibrary_Adminhtml_IndexController extends Mage_Adminhtml_Controller_Action
{
		
	/**
	 *  Init action
	 * 
	 * @return MageParts_MediaLibrary_Adminhtml_IndexController
	 */
	public function _initAction()
	{
		// @var MageParts_MediaLibrary_Helper_Data
		$helper = Mage::helper('medialibrary');
		
		$this->loadLayout()
			->_setActiveMenu('cms')
			->_addBreadcrumb($helper->__('CMS'), $helper->__('CMS'))
			->_addBreadcrumb($helper->__('Media Library'), $helper->__('Media Library'));
		return $this;
	}
	
	
	/**
	 * Index action
	 */
	public function indexAction()
	{
		// initialize
		$this->_initAction();
		
		// this is to ensure that we may load ext js (required for the file tree to work)
		$this->getLayout()->getBlock('head')->setCanLoadExtJs(true);
		
		// add the file tree to the left-hand side
		 $this->_addLeft(
            $this->getLayout()->createBlock('medialibrary/adminhtml_tree', 'medialibrary.tree')
        );
        
        // add the edit area to the content side
        $this->_addContent(
            $this->getLayout()->createBlock('medialibrary/adminhtml_edit')
        );
		
        // render layout
		$this->renderLayout();
	}
	
	
	/**
	 * Upload action
	 */
	public function uploadAction()
	{
		// @var MageParts_MediaLibrary_Model_Files
		$model = Mage::getSingleton('medialibrary/files');
		
		// @var MageParts_MediaLibrary_Helper_Data
		$helper = Mage::helper('medialibrary');
		
		// @var MageParts_MediaLibrary_Helper_Validate
		$validateHelper = Mage::helper('medialibrary/validate');
		
		// upload results
        $result = array();

        try {
        	// get the current working path (this is the path to which we intend to upload)
        	$dir = $model->getAbsolutePath($this->getRequest()->getPost('uploadDest'));
        	
        	// validate upload directory
        	$validateHelper->validateDirPath($dir);
    
        	// upload directory must be writable
        	if(!is_writable($dir)) {
        		Mage::throwException($helper->__("Unable to upload file(s). Upload Destination path is not writable."));
        	}
        	
        	// create the uploader object 
            $uploader = new Varien_File_Uploader('file');
            $uploader->setAllowRenameFiles(true);
            $uploader->setFilesDispersion(false);
            $uploader->setAllowedExtensions($model->getAllFiltersAsArray());
            
            // clean the filename from illegal characters before saving
            $filename = $model->getValidFilename(str_replace(' ','_',$_FILES['file']['name']));
            
            // save the file
            $result = $uploader->save($dir,$filename);
            
            // set the classname of the file, this will be used by the file tree to display the proper file icon
            $result['cls'] = $model->getIconClassByFileType($_FILES['file']['name']);
            $result['type'] = $result['cls'];
        } 
        catch (Exception $e) {
            $result = array('error'=>$e->getMessage(), 'errorcode'=>$e->getCode());
        }

        // return JSON encoded response
        $this->getResponse()->setBody(Zend_Json::encode($result));
	}
	
	
	/**
	 * Move a file
	 */
	public function moveAction()
	{
		// @var MageParts_MediaLibrary_Model_Files
		$model = Mage::getSingleton('medialibrary/files');
		
		// @var MageParts_MediaLibrary_Helper_Data
		$helper = Mage::helper('medialibrary');
		
		// @var MageParts_MediaLibrary_Helper_Validate
		$validateHelper = Mage::helper('medialibrary/validate');
		
		try {
			/*  - POST DATA -  */
			$srcPath 	= $helper->getSrcPathParam();												// current file path
			$destPath 	= $helper->getDestPathParam();												// new file path
			$filename 	= $model->getValidFilename($this->getRequest()->getPost('filename'));		// name of file
			$replace 	= $helper->getReplaceParam();												// whether or not to replace existing file with equal name
	    	
			// validate source file path
			$srcFile = ($srcPath . DS . $filename);
			$validateHelper->validatePath($srcFile);
			
			// check if the src path orgins from the trashcan
			if($model->getTrashcanPath() == $srcPath) {
				$destFile = ($destPath . DS . $model->getValidFilename(substr($filename,strpos($filename,'_')+1)));
			}
			else {
				$destFile = ($destPath . DS . $filename);
			}
			
			// validate destination file path
			$validateHelper->validatePath($destFile,false);
			
			// check if the file already exists in the selected path
			if($destFile !== $srcFile) {
				// check if the destination path already exists
				if(file_exists($destFile) && !$replace) {
					if(is_dir($destFile)) {
						$this->getResponse()->setBody("EXISTS-DIR");
					}
					else {
						$this->getResponse()->setBody("EXISTS");
					}
					return;
				}
				
				// attempt to move the file / directory
				if($model->move($srcFile,$destFile,$replace)) {
					// success
					$this->getResponse()->setBody("SUCCESS");
					return;
				}
			}
		}
		catch (Exception $e) {
			$this->getResponse()->setBody($e->getMessage());
			return;
		}
	}
	
	
	/**
	 * Save a file
	 */ 
	public function saveAction()
	{
		// @var MageParts_MediaLibrary_Model_Files
		$model = Mage::getSingleton('medialibrary/files');
		
		// @var MageParts_MediaLibrary_Helper_Data
		$helper = Mage::helper('medialibrary');
		
		// @var MageParts_MediaLibrary_Helper_Validate
		$validateHelper = Mage::helper('medialibrary/validate');
		
		try {
			/*  - POST DATA -  */
			$srcPath 		= $helper->getSrcPathParam();				// current file path
			$srcFilename 	= $helper->getSrcFilenameParam();			// current filename
			$destFilename 	= $helper->getDestFilenameParam();			// updated filename
			$filetype 		= $helper->getFileTypeParam();				// file type
			$replace 		= $helper->getReplaceParam();				// whether or not to replace existing file with equal name
			
			// validate filename extensions
			if($filetype != 'folder') {
				$validateHelper->validateFilenameExtension($srcFilename);
				$validateHelper->validateFilenameExtension($destFilename);	
			}
			
			// validate source file
			$srcFile = ($srcPath . DS . $srcFilename);
			$validateHelper->validatePath($srcFile);
			
			// validate destination file
			$destFile = ($srcPath . DS . $destFilename);
			$validateHelper->validatePath($destFile,false);
			
			// rename the file if need be
			if($srcFile != $destFile) {
				// check if the destination path already exists
				if(file_exists($destFile) && !$replace) {
					if(is_dir($destFile)) {
						$this->getResponse()->setBody("EXISTS-DIR");
					}
					else {
						$this->getResponse()->setBody("EXISTS");
					}
					return;
				}

				// attmpt to rename the file (we will use the move function to do so)
				$model->move($srcFile,$destFile,$replace);
			}
			
			// image specific rules
			if($filetype == 'image') {
				/* resize image */
				$newWidth  = intval($this->getRequest()->getPost('width'));		// image width
				$newHeight = intval($this->getRequest()->getPost('height'));	// image height
	
				// resize image
				Mage::getModel('medialibrary/files_image')->resize($destFile, $newWidth, $newHeight);
			}
			
			// success
			$this->getResponse()->setBody("SUCCESS");
			return;
		}
		catch (Exception $e) {
			$this->getResponse()->setBody($e->getMessage());
			return;
		}
	}
	
	
	/**
	 * Add new directory
	 */
	public function createDirAction()
	{
		// @var MageParts_MediaLibrary_Model_Files
		$model = Mage::getSingleton('medialibrary/files');
		
		// @var MageParts_MediaLibrary_Helper_Data
		$helper = Mage::helper('medialibrary');
		
		// @var MageParts_MediaLibrary_Helper_Validate
		$validateHelper = Mage::helper('medialibrary/validate');
		
		try {
			/*  - POST DATA -  */
			$path 	 = $model->getAbsolutePath($this->getRequest()->getPost('path'));					// parent directory path
			$dirname = $model->getValidFilename($this->getRequest()->getPost('dir'));				// new directory name
			$replace = $helper->getReplaceParam();														// whether or not to replace existing directory with equal name

			// $dirname may not be empty
			if(empty($dirname)) {
				Mage::throwException($helper->__("No name was supplied for the new directory. Please make sure that you provided a valid directory name and try again."));	
			}
			
			// validate path where the directory will be created
			$validateHelper->validateDirPath($path);
			
			// validate destination path
			$dir = ($path . DS .$dirname);
			$validateHelper->validateDirPath($dir,false);
			
			// check if the directory already exists
			if(is_dir($dir)) {
				// attempt to replace the directory
				if(!$replace) {
					$this->getResponse()->setBody("EXISTS-DIR");
					return;
				}
				else if(!$model->remove($dir,true)) {
					Mage::throwException($helper->__("Unable to replace existing directory"));
				}
			}
			
			// create the new directory
			if($model->makeDir($dir)) {
				$this->getResponse()->setBody("SUCCESS");
			}
			else {
				Mage::throwException($helper->__("Unable to create the requested directory"));
			}
		}
		catch (Exception $e) {
			$this->getResponse()->setBody($e->getMessage());
			return;
		}
	}

	
	/**
	 * Delete a file or folder
	 */
	public function deleteAction()
	{
		// @var MageParts_MediaLibrary_Model_Files
		$model = Mage::getSingleton('medialibrary/files');
		
		// @var MageParts_MediaLibrary_Helper_Data
		$helper = Mage::helper('medialibrary');
		
		// @var MageParts_MediaLibrary_Helper_Validate
		$validateHelper = Mage::helper('medialibrary/validate');
		
		try {
			/*  - POST DATA -  */
			$srcPath  = $helper->getSrcPathParam();				 	// current file path
			$filename = $this->getRequest()->getPost('filename'); 	// file to be deleted
			
			// validate complete source file path
			$srcFile = ($srcPath . DS . $filename);
			$validateHelper->validatePath($srcFile);
		
			// check if the trashcan is enabled
			if(Mage::getStoreConfig('medialibrary/trashcan/enabled',Mage::helper('medialibrary')->getStoreId())) {
				// attempt to get trashcan path
				if(!($trashcanPath = $model->getTrashCanPath())) {
					Mage::throwException($helper->__("Unable to retrieve trashcan path"));
				}
				
				// filename in trashcan
				$newFilename = time() . '_' . $filename;
				
				// validate destination file
				$destFile = ($trashcanPath . DS . $newFilename);
				$validateHelper->validatePath($destFile,false);

				// make sure that the file to be deleted exists
				if($model->move($srcFile,$destFile,false)) {
					// success
					$this->getResponse()->setBody("SUCCESS|{$newFilename}");
					return;
				}
			}
			else {
				// attempt to remove the file
				if($model->remove($srcFile,true)) {
					// success
					$this->getResponse()->setBody("SUCCESS");
					return;
				}
			}
		}
		catch (Exception $e) {
			$this->getResponse()->setBody($e->getMessage());
			return;
		}
	}
	
	
	/**
	 * Copy a file or folder
	 */
	public function copyAction()
	{
		// @var MageParts_MediaLibrary_Model_Files
		$model = Mage::getSingleton('medialibrary/files');
		
		// @var MageParts_MediaLibrary_Helper_Data
		$helper = Mage::helper('medialibrary');
		
		// @var MageParts_MediaLibrary_Helper_Validate
		$validateHelper = Mage::helper('medialibrary/validate');
		
		try {
			/*  - POST DATA -  */
			$srcPath 	= $helper->getSrcPathParam();												// current file path
			$destPath 	= $helper->getDestPathParam();												// new file path
			$filename 	= $model->getValidFilename($this->getRequest()->getPost('filename'));		// name of file
			$replace 	= $helper->getReplaceParam();												// whether or not to replace existing file with equal name

			// validate source file path
			$srcFile = ($srcPath . DS . $filename);
			$validateHelper->validatePath($srcFile);
			
			// validate file destination path
			$destFile = ($destPath . DS . $filename);
			$validateHelper->validatePath($destFile,false);
			
			/*  - CORRECT DESTINATION FILE, AVOID REPLACEMENTS -  */
			$fileExt 		= '';		// file extension
			$fileShortName 	= '';		// filename minus file extension
			
			if(is_dir($srcFile)) {
				$fileShortName = $filename;
			}
			else if(is_file($srcFile)) {
				$validateHelper->validateFilenameExtension($filename);
				
				$fileExt		= substr($filename,strrpos($filename,'.')+1);
				$fileShortName 	= substr($filename,0,strrpos($filename,'.'));
			}
			else {
				Mage::throwException($helper->__("Invalid source file, source file doesn't exist"));
			}
				
			$index = 1;
			
			// if the destination file already exists, add _$index to destination filename
			while (file_exists($destFile)) {
				$filename = !is_dir($srcFile) ? ($fileShortName . '_' . ++$index . '.' . $fileExt) : ($fileShortName . '_' . ++$index);
				$destFile = ($destPath . DS . $filename);
			}
			
			// validate file destination path again, it may have changed
			$destFile = ($destPath . DS . $filename);
			$validateHelper->validatePath($destFile,false);
			
			// source file must be in root directory
			if(!$model->isInRootDirectory($srcFile)) {
				Mage::throwException($helper->__("Invalid source file. The source file most exists within one of your root directories."));
			}
			
			// destination path must be in root directory
			if(!$model->isInRootDirectory($destPath)) {
				Mage::throwException($helper->__("Invalid destination file path. The destination file path most exists within one of your root directories."));
			}

			// attempt to copy the file / directory
			if($model->_copy($srcFile,$destFile)) {
				// success
				$this->getResponse()->setBody("SUCCESS|{$filename}");
				return;
			}
		}
		catch (Exception $e) {
			$this->getResponse()->setBody($e->getMessage());
			return;
		}
	}
	
	
	/**
	 * Empty the trashcan
	 */
	public function emptyTrashcanAction()
	{
		// @var MageParts_MediaLibrary_Model_Files
		$model = Mage::getSingleton('medialibrary/files');

		// @var MageParts_MediaLibrary_Helper_Validate
		$validateHelper = Mage::helper('medialibrary/validate');
		
		try {
			// get trashcan path
			if($trashcan = $model->getTrashCanPath()) {
				// validate the trashcans path
				$validateHelper->validateDirPath($trashcan);
				
				// attempt to open the trashcan path
				if($dir = opendir($trashcan)) {
					// remove all files inside of the trashcan
					while(false !== ($file = readdir($dir))) {
						// ignore "." & ".."
						if(($file != '.') && ($file != '..')) {
	    					$model->remove(($trashcan . DS . $file),true);
	    				}
					}
				}
			}
			
			// success
			$this->getResponse()->setBody("SUCCESS");
			return;
		}
		catch (Exception $e) {
			$this->getResponse()->setBody($e->getMessage());
			return;
		}
	}
	
	
	/**
	 * Compress one or more files
	 */
	public function compressAction()
	{
		// @var MageParts_MediaLibrary_Model_Files
		$model = Mage::getSingleton('medialibrary/files');
		
		// @var MageParts_MediaLibrary_Helper_Data
		$helper = Mage::helper('medialibrary');
		
		// @var MageParts_MediaLibrary_Helper_Validate
		$validateHelper = Mage::helper('medialibrary/validate');
		
		// variable for future archive reference
		$archive = '';
		
		try {
			/*  - POST DATA -  */
			$fileList		= $this->getRequest()->getPost('fileList');  	// list of files to be compressed
			$method 		= $this->getRequest()->getPost('method');		// compression method
			$landingPath 	= $model->getAbsolutePath($this->getRequest()->getPost('path'));			// landing path of the compressed file

			// vlaidate filelist
			if(empty($fileList)) {
				Mage::throwException($helper->__("File list is empty, nothing to do."));
			}
			
			// validate method
			$validateHelper->validateCompressionMethod($method);
			
			// validate path where the compressed file will be saved
			$validateHelper->validateDirPath($landingPath);
			
			// files array
			$files = explode('|',$fileList);
			
			// attempt to compress the files
			if($archive = $model->compressFiles($files)) {
				// create zip file name
				$firstFilePath = $model->getAbsolutePath($files[0]);
				
				// get zip filename / path
				$filename = sizeof($files)>1 || is_file($firstFilePath) ? "files.{$method}" : substr($firstFilePath,strrpos($firstFilePath,'/')+1) . '.' . $method;
				$filepath = realpath($landingPath) . DS . $filename;
				
				// index count
				$index = 1;

				// check if landing file path already exists
				while (file_exists($filepath)) {
					// add _$index before the file extension
					$filename = sizeof($files)>1 || is_file($firstFilePath) ? "files_{$index}.{$method}" : substr($firstFilePath,strrpos($firstFilePath,'/')+1) . '_' . $index . '.' . $method;
					$filepath = realpath($landingPath) . DS . $filename;
	
					$index++;
				}

				// move the archive file to the landing path
				if(!$model->move($archive,$filepath,false)) {
					Mage::throwException($helper->__("Unable to complete the compression operation. The compressed archive could not be moved to the requested destination path."));
				}

				// success
				$this->getResponse()->setBody("SUCCESS|{$filename}");
				return;
			}
		}
		catch (Exception $e) {
			// clean up
			if(!empty($archive)) {
				$model->remove($archive);
			}
					
			// failure
			$this->getResponse()->setBody($e->getMessage());
			return;
		}
		return;
	}
		
	
	/**
	 * Extract one or more files
	 */
	public function extractAction()
	{
		// @var MageParts_MediaLibrary_Model_Files
		$model = Mage::getSingleton('medialibrary/files');
		
		// @var MageParts_MediaLibrary_Helper_Data
		$helper = Mage::helper('medialibrary');
		
		// @var MageParts_MediaLibrary_Helper_Validate
		$validateHelper = Mage::helper('medialibrary/validate');
		
		try {
			/*  - POST DATA -  */
			$file = $model->getAbsolutePath($this->getRequest()->getPost('file')); 		// file to be extracted
			
			// validate file path
			$validateHelper->validatePath($file);
			
			// attempt to extract the file
			if($archive = $model->extractFile($file)) {
				// success
				$this->getResponse()->setBody("SUCCESS");
				return;
			}
		}
		catch (Exception $e) {	
			// failure
			$this->getResponse()->setBody($e->getMessage());
			return;
		}
		return;
	}
	
	
	/**
	 *  Make sure the user can access this controller
	 */
	protected function _isAllowed()
	{
		return Mage::getSingleton('admin/session')->isAllowed('cms/medialibrary');
	}
	
}