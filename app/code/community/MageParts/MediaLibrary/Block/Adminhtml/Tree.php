<?php
/**
 * MageParts
 * 
 * NOTICE OF LICENSE
 * 
 * This code is copyrighted by MageParts and may not be reproduced
 * and/or redistributed without a written permission by the copyright 
 * owners. If you wish to modify and/or redistribute this file please
 * contact us at info@mageparts.com for confirmation before doing
 * so. Please note that you are free to modify this file for personal
 * use only.
 *
 * If you wish to make modifications to this file we advice you to use
 * the "local" file scope in order to aviod conflicts with future updates. 
 * For information regarding modifications see http://www.magentocommerce.com.
 *  
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF 
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY 
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * @category   MageParts
 * @package    MageParts_MediaLibrary
 * @copyright  Copyright (c) 2009 MageParts (http://www.mageparts.com/)
 * @author 	   MageParts Crew
 */

class MageParts_MediaLibrary_Block_Adminhtml_Tree extends Mage_Adminhtml_Block_Template
{
	
	/**
	 * Holds the current node id
	 *
	 * @var int
	 */
	private $_nodeId = 0;
	
	
	/**
	 * Wheter standalone was requested or not
	 * 
	 * @var boolean
	 */
	private $_standalone = null;
	
	
	/**
	 * Filter
	 * 
	 * @var mixed
	 */
	private $_filters = null;
	
	
	/**
	 * Filter array
	 * 
	 * @var array
	 */
	private $_filterArray = array();
	
	
	/**
	 * URL of file to be auto selected
	 *
	 * @var string
	 */
//	private $_autoSelectedFile;
	
	
	/**
	 * Returns whether or not a node has been auto selected
	 * 
	 * @return boolean
	 */
//	private $_hasAutoSelectedNode = false;
	

	
	/**
	 * Constructor
	 */
    public function __construct()
    {
    	// call parent constructor
        parent::__construct();
        
        // set block template
        $this->setTemplate('mageparts/medialibrary/tree.phtml');
        $this->setUseAjax(true);
    }
    
    
    /**
     * Prepare layout
     *
     * @return Mage_Adminhtml_Block_Template
     */
    protected function _prepareLayout()
    {
    	// create new directory button
        $this->setChild('create_directory_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => Mage::helper('medialibrary')->__('Create New Directory'),
                    'onclick'   => "medialibrary.createDir()",
                    'class'     => 'add'
                ))
        );
        
        // execute multi action button
        $this->setChild('execute_multi_action_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => Mage::helper('medialibrary')->__('Execute'),
                    'onclick'   => "medialibrary.multiAction()",
                    'class'     => 'save'
                ))
        );
        
        return parent::_prepareLayout();
    }

    
    /**
     * Return HTML for the create directory button
     *
     * @return string
     */
    public function getCreateDirectoryButtonHtml()
    {
        return $this->getChildHtml('create_directory_button');
    }
    
    
    /**
     * Return HTML for the execute multi action button
     *
     * @return string
     */
    public function getExecuteMultiActionButtonHtml()
    {
        return $this->getChildHtml('execute_multi_action_button');
    }
    
    
    /**
     * Return JSON encoded data for the file tree
     *
     * @return string
     */
    public function getFileTreeJson()
    {
    	// file tree container
    	$arr = array();
		
    	// add each root directory (and subdirectories / files) to the file tree array
    	foreach (Mage::getSingleton('medialibrary/files')->getRootDirectories() as $r) {
	 		$arr2 = $this->sortTree($this->getFileTree($r));
	 		$arr = array_merge($arr,$arr2);
    	}
   
    	// try to add the trashcan to the file tree (as a root node)
    	if(Mage::getStoreConfig('medialibrary/trashcan/enabled',Mage::helper('medialibrary')->getStoreId())) {
	    	if($trashcan = Mage::getSingleton('medialibrary/files')->getTrashCanPath()) {
		    	$arr = array_merge($arr,$this->sortTree($this->getFileTree($trashcan,0,true,true)));
	    	}
    	}
    	
    	// JSON encode the file tree array
    	$json = Zend_Json::encode($arr);
    	
    	// retrun the JSON encoded file tree
    	return $json;
    }
    
    
    /**
     * Read data from selected directories and order said data
     * as an array
     *
     * @param string $dir
     * @param int $level [optional]
     * @param boolean $includeRoot [optional]
     * @return array
     */
    protected function getFileTree( $dir, $level=0, $includeRoot=true, $isTrashcan=false )
    {
    	// @var MageParts_MediaLibrary_Model_Files
    	$model = Mage::getSingleton('medialibrary/files');
    	
    	// result array
    	$arr = array();
    	
    	// get realpath of dir
    	$dir = realpath($dir);
    	
    	// check for file filter(s)
    	if(is_null($this->_filter)) {
    		if($this->getRequest()->getParam('types')) {
	    		$types = explode(',',trim($this->getRequest()->getParam('types')));
	    		$this->_filters = (!$types || in_array('*',$types)) ? array() : $types;
    		}
    	}
    	
    	// first level nodes only, may only be directories
    	if($level == 0) {
    		// make sure that the first level is a directory
    		if($model->isValidDir($dir)) {
    			// increase node id
	    		$this->_nodeId++;
	    		
	    		// node url attribute
	    		$url = substr($dir,strpos($dir,Mage::getBaseDir('base'))+strlen(Mage::getBaseDir('base')));
				$url = str_replace('/index.php/','',Mage::getBaseUrl()).$url;
				
				// specific trashcan rules
				if($isTrashcan && ($dir == $model->getTrashCanPath())) {
					$isEmpty = true;
					
					// check if the trashcan is empty
					if($trashcanDir = opendir($dir)) {
						while(false !== ($file = readdir($trashcanDir))) {
							if(($file != '.') && ($file != '..')) {
								$isEmpty = false;
								break;
							}
						}
					}

					// trashcan node array
					if($trashcanNodePath = $model->getRelativePath($dir)) { 
						$arr = array(
				    		array(
				    			"text" 		=> Mage::helper('medialibrary')->__("Trashcan"),
				    			"id"		=> 'tree_node_trashcan',
				    			"path" 		=> dirname($trashcanNodePath),
				    			"filename"	=> substr($trashcanNodePath,strrpos($trashcanNodePath,'/')),
				    			"url"		=> $url,
				    			"size"		=> '0 KB',
				    			"cls" 		=> !$isEmpty ? "folder trashcan" : "folder trashcan empty",
				    			"type"		=> "folder",
			    				"allowDrop" => true,
			    				"allowDrag" => false,
			    				"children" 	=> array(),
			    				"editable"	=> false,
			    				"inTrash"	=> false
				    		)
				    	);
					}
				}
				else {
					// check if the node should be auto selected
/*					$nodeId = 'tree_node_'.$this->_nodeId;
					if(!$this->_hasAutoSelectedNode && $this->getAutoSelectedFile()==$url) {
						$nodeId = 'tree_node_auto_selected';
						$this->_hasAutoSelectedNode = true;
					}
*/					
					// root directory array
					if($rootNodePath = $model->getRelativePath($dir)) { 
				    	$arr = array(
				    		array(
				    			"text" 		=> substr($rootNodePath,strrpos($rootNodePath,'/')+1),
				    			"id"		=> 'tree_node_'.$this->_nodeId,
				    			"path" 		=> dirname($rootNodePath),
				    			"filename"	=> substr($rootNodePath,strrpos($rootNodePath,'/')),
				    			"url"		=> $url,
				    			"size"		=> '0 KB',
				    			"cls" 		=> "folder",
				    			"type"		=> "folder",
			    				"allowDrop" => true,
			    				"allowDrag" => false,
			    				"children" 	=> array(),
			    				"editable"	=> false,
			    				"inTrash"	=> false
				    		)
				    	);
					}
				}
    		}
    	}

    	// check that the requested dir to be read exists
    	if($model->isValidDir($dir)) {
    		// read directory
    		if($handle = opendir($dir)) {
	    		while (false !== ($file = readdir($handle))) {
	    			// files may not begin with a dott (.)
	    			if(substr($file,0,1)!=='.' && $file != '.' && $file != '..') {
	    				// file url
	    				$currentFile = ($dir . DS .$file);
	    				// file is directory
	    				$isDir = is_dir($currentFile);
	    				
	    				// validate file path
	    				if($isDir) {
	    					if(!$model->isValidDir($currentFile)) {
	    						continue;
	    					}
	    				}
	    				else {
	    					if(!$model->isValidPath($currentFile)) {
	    						continue;
	    					}
	    				}
	    				
	    				// check for filters
	    				if($this->_filters && !$isDir) {
	    					$filterOk = false;
	    					$ext = substr($file,strrpos($file,'.')+1);
	    					
	    					// Get filter array
	    					foreach ($this->_filters as $filter) {
		    					if(!isset($this->_filterArray[$filter])) {
		    						$this->_filterArray[$filter] = explode(',',Mage::getStoreConfig('medialibrary/'.$filter.'/filter',Mage::helper('medialibrary')->getStoreId()));
		    					}
	
		    					if(in_array($ext,$this->_filterArray[$filter])) {
		    						// file extension was ok, include file
		    						$filterOk = true;
		    						break;
		    					}
	    					}
	    					
	    					// file extension was not ok, exclude file
	    					if(!$filterOk) {
	    						continue;
	    					}
	    				}
	    				
	    				// increase node id
	    				$this->_nodeId++;

	    				// node url attribute
	    				$url = substr($currentFile,strpos($currentFile,Mage::getBaseDir('base'))+strlen(Mage::getBaseDir('base')));
    					$url = str_replace('/index.php/','',Mage::getBaseUrl()).$url;
						
    					// node class attribute
    					$cls = $isDir ? "folder" : $model->getIconClassByFileType($file);
    					
    					// node filetype attribute
    					$fileType = $cls;
    					
    					// if the node is located within the trashcan, add the class name "disabled" to the node
    					$cls.= $isTrashcan ? " disabled" : "";
    					
    					// check if the node should be auto selected
/*						$nodeId = 'tree_node_'.$this->_nodeId;
						if(!$this->_hasAutoSelectedNode && $this->getAutoSelectedFile()==$url) {
							$nodeId = 'tree_node_auto_selected';
							$this->_hasAutoSelectedNode = true;
						}
*/    					
    					// node array
    					if($fileNodePath = $model->getRelativePath($dir)) {
			    			$tmp = array(
			    				"text" 		=> (!$isTrashcan || $level>0) ? $file : substr($file,strpos($file,'_')+1),
			    				"id" 		=> 'tree_node_'.$this->_nodeId,
			    				"path" 		=> $fileNodePath,
			    				"filename"	=> $file,
			    				"url" 		=> $url,
			    				"size"		=> !$isDir ? $this->size_readable(filesize($currentFile)) : '0 KB',
			    				"cls" 		=> $cls,
			    				"type"		=> $fileType,
			    				"allowDrop" => true,
			    				"allowDrag" => true,
			    				"children" 	=> $isDir ? $this->getFileTree($currentFile,$level+1,$includeRoot,$isTrashcan) : "",
			    				"editable"	=> !$isTrashcan ? true : false,
			    				"inTrash"	=> $isTrashcan ? true : false
			    			);
		    			
			    			// special image attributes
			    			if(str_replace(' disabled','',$cls) == 'image') {
			    				$imageSize = getimagesize($currentFile);
			    				$tmp['width'] = $imageSize[0];
			    				$tmp['height'] = $imageSize[1];
			    			}
			    			
			    			// decide wheter or not the node is a leaf (leaf = file)
			    			if(!$isDir) {
			    				$tmp["leaf"] = true;
			    			}
			    			
			    			// if this was a first level node the read-out are children of that node
			    			if($level == 0) {
			    				$arr[0]['children'][] = $tmp;
			    			}
			    			else {
			    				$arr[] = $tmp;
			    			}
    					}
	    			}
			    }
    		}
    	}
    
    	// return results
    	return $arr;
    }
    
    
    /**
     * Sorts the data which is to be used in the file tree
     *
     * @param array $arr
     * @return array
     */
    public function sortTree( array $arr )
    {
    	$folders = array();
    	$files 	 = array();
    	
    	foreach ($arr as $node) {
    		if($node['cls'] == 'folder') {
    			if($node['children']) {
    				$node['children'] = $this->sortTree($node['children']);
    			}

    			$folders[] = $node;
    		}
    		else {
    			$files[] = $node;
    		}
    	}
    	
    	usort($folders,array($this,"cmp"));
    	usort($files,array($this,"cmp"));
    	
    	return array_merge_recursive($folders,$files);
    }
    

    /**
     * String comparison function. Used by the 
     * sortTree function
     *
     * @param string $a
     * @param string $b
     * @return int
     */
	public function cmp( $a, $b )
	{
	   return strcmp($a['text'], $b['text']);
	}
    
	
	/**
	 * Get url to move action
	 * 
	 * @return string
	 */
    public function getMoveUrl()
    {
    	return $this->getUrl('*/*/move');
    }
     
	
	/**
	 * Get url to save action
	 * * 
	 * @return string
	 */
    public function getSaveUrl()
    {
    	return $this->getUrl('*/*/save');
    }
    
    
    /**
     * Get url to delete action
     * 
	 * @return string
     */
    public function getDeleteUrl()
    {
    	return $this->getUrl('*/*/delete');
    }
    
    
    /**
     * Get url to copy action
     * 
	 * @return string
     */
    public function getCopyUrl()
    {
    	return $this->getUrl('*/*/copy');
    }
    
    
    /**
     * Get url to createDir action
     * 
	 * @return string
     */
    public function getCreateDirUrl()
    {
    	return $this->getUrl('*/*/createDir');
    }
      

    /**
     * Return url to emptyTrashcan action 
     * 
	 * @return string
     */
    public function getEmptyTrashcanUrl()
    {
    	return $this->getUrl('*/*/emptyTrashcan');
    }
      
  
    /**
     * Return url to compress action 
     * 
	 * @return string
     */
    public function getCompressUrl()
    {
    	return $this->getUrl('*/*/compress');
    }
          
  
    /**
     * Return url to extract action 
     * 
	 * @return string
     */
    public function getExtractUrl()
    {
    	return $this->getUrl('*/*/extract');
    }
    
    
    /**
     * Check if standalone was requested
     * 
	 * @return boolean
     */
    public function isStandalone()
    {
    	if(is_null($this->_standalone)) {
    		$this->_standalone = (bool)$this->getRequest()->getParam('standalone');
    	}
    	return $this->_standalone;
    }
        
    
    /**
     * Return file to be auto selected
     * 
     * @return string
     */
/*    public function getAutoSelectedFile()
    {
    	if(is_null($this->_autoSelectedFile)) {
    		$this->_autoSelectedFile = str_replace('|','/',urldecode($this->getRequest()->getParam('selectedFile')));
    	}
    	return $this->_autoSelectedFile;
    }
*/
  
    
   /**
	* Return human readable sizes
	*
	* @author      Aidan Lister <aidan@php.net>
	* @version     1.2.0
	* @link        http://aidanlister.com/repos/v/function.size_readable.php
	* @param       int     $size        size in bytes
	* @param       string  $max         maximum unit
	* @param       bool    $si          use SI (1000) prefixes
	* @param       string  $retstring   return string format
	*/
	public function size_readable($size, $max = null, $si = true, $retstring = '%01.2f %s')
	{
	    // Pick units
		if ($si === true) {
			$sizes = array('B', 'KB', 'MB', 'GB', 'TB', 'PB');
			$mod   = 1000;
		 } else {
			$sizes = array('B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB');
			$mod   = 1024;
		}

		// Max unit to display
		if ($max && false !== $d = array_search($max, $sizes)) {
			$depth = $d;
		} else {
			$depth = count($sizes) - 1;
		}

		// Loop
		$i = 0;
		while ($size >= 1024 && $i < $depth) {
			$size /= $mod;
			$i++;
		}

		return sprintf($retstring, $size, $sizes[$i]);
	}
    
}