<?php
/**
 * MageParts
 * 
 * NOTICE OF LICENSE
 * 
 * This code is copyrighted by MageParts and may not be reproduced
 * and/or redistributed without a written permission by the copyright 
 * owners. If you wish to modify and/or redistribute this file please
 * contact us at info@mageparts.com for confirmation before doing
 * so. Please note that you are free to modify this file for personal
 * use only.
 *
 * If you wish to make modifications to this file we advice you to use
 * the "local" file scope in order to aviod conflicts with future updates. 
 * For information regarding modifications see http://www.magentocommerce.com.
 *  
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF 
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY 
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * @category   MageParts
 * @package    MageParts_MediaLibrary
 * @copyright  Copyright (c) 2009 MageParts (http://www.mageparts.com/)
 * @author 	   MageParts Crew
 */

class MageParts_MediaLibrary_Block_Adminhtml_Edit extends Mage_Adminhtml_Block_Template
{

	/**
	 * Wheter popup was requested or not
	 * 
	 * @var boolean
	 */
	private $_standalone = null;
	
	
	/**
	 * Requested file types filter
	 * 
	 * @var string
	 */
	private $_types = null;
	
	
	
	/**
	 * Constructor
	 */
    public function __construct()
    {
        $this->_controller  = 'index';
        $this->_blockGroup 	= 'medialibrary';

        // run parent constructor
        parent::__construct();

        // set block template
        $this->setTemplate('mageparts/medialibrary/edit.phtml');
    }
    
    
    /**
     * Prepare layout
     */
    protected function _prepareLayout()
    {
    	// delete button
    	$this->setChild('delete_media_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => Mage::helper('medialibrary')->__('Delete'),
                    'onclick'   => "medialibrary.del()",
                    'class'     => 'delete action-button'
                ))
        );
        
        // use button
        $this->setChild('use_media_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => Mage::helper('medialibrary')->__('Use'),
                    'onclick'   => "FileBrowserDialogue.use()",
                    'class'     => 'save action-button'
                ))
        );

        // save button
        $this->setChild('save_media_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => Mage::helper('medialibrary')->__('Save'),
                    'onclick'   => "medialibrary.save()",
                    'class'     => 'save action-button'
                ))
        );
        
        // copy button
        $this->setChild('copy_file_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => Mage::helper('medialibrary')->__('Copy'),
                    'onclick'   => "medialibrary.copy()",
                    'class'		=> 'action-button'
                ))
        );
        
        // paste button
        $this->setChild('paste_file_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => Mage::helper('medialibrary')->__('Paste'),
                    'onclick'   => "medialibrary.paste()",
                    'class'		=> 'action-button'
                ))
        );
        
        // download button
        $this->setChild('download_file_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => Mage::helper('medialibrary')->__('Download'),
                    'onclick'   => "medialibrary.downloadFile()",
                    'class'		=> 'action-button download-button'
                ))
        );
        
        // empty trashcan button
        $this->setChild('empty_trashcan_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => Mage::helper('medialibrary')->__('Empty Trashcan'),
                    'onclick'   => "medialibrary.emptyTrashcan()",
                    'class'		=> 'action-button empty-trashcan-button'
                ))
        );
          
        // compress button      
        $this->setChild('compress_file_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => Mage::helper('medialibrary')->__('Compress'),
                    'onclick'   => "medialibrary.compress(false,'zip',true)",
                    'class'		=> 'action-button'
                ))
        );
          
        // extract button     
        $this->setChild('extract_file_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => Mage::helper('medialibrary')->__('Extract'),
                    'onclick'   => "medialibrary.extract()",
                    'class'		=> 'action-button'
                ))
        );
    	
        // flash uploader object
        $this->setChild('uploader',
            $this->getLayout()->createBlock('adminhtml/media_uploader')
        );

        // get upload filters
        $filters = Mage::getSingleton('medialibrary/files')->getAllFiltersAsArray(true);
        $filtersArr = array();
        
        if(is_null($this->_types)) {
        	$this->_types = $this->getRequest()->getParam('types') ? explode(',',$this->getRequest()->getParam('types')) : array();
        }
        
        foreach ($filters as $type=>$exts) {
			if(in_array($type,$this->_types) || in_array('*',$this->_types) || !$this->_types) {
	        	$files = array();
	        	
	        	foreach ($exts as $ext) {
	        		$files[] = "*.{$ext}";
	        	}
	        	
	        	$filtersArr[$type] = array(
	        		'label' => Mage::helper('medialibrary')->__('%s files (.%s)',ucfirst($type),implode(', .',$exts)),
	        		'files' => $files
	        	);
			}
        }
        
        // apply uploader config
        $this->getUploader()->getConfig()
            ->setUrl(Mage::getModel('adminhtml/url')->addSessionParam()->getUrl('*/adminhtml_index/upload'))
            ->setFilters($filtersArr);

        return parent::_prepareLayout();
    }

    
    /**
     * Retrieve HTML for the save media button
     *
     * @return string
     */
    public function getSaveMediaButtonHtml()
    {
        return $this->getChildHtml('save_media_button');
    }
    
    
    /**
     * Retrieve HTML for the use media button
     *
     * @return string
     */
    public function getUseMediaButtonHtml()
    {
    	if($this->isStandalone()) {
       		return $this->getChildHtml('use_media_button');
    	}
    	return;
    }
   
    
    /**
     * Retrieve HTML for the delete media button
     *
     * @return string
     */
    public function getDeleteMediaButtonHtml()
    {
        return $this->getChildHtml('delete_media_button');
    }
       
    
    /**
     * Retrieve HTML for the copy file button
     *
     * @return HTML sring
     */
    public function getCopyFileButtonHtml()
    {
        return $this->getChildHtml('copy_file_button');
    }
           
    
    /**
     * Retrieve HTML for the download file button
     *
     * @return string
     */
    public function getDownloadFileButtonHtml()
    {
        return $this->getChildHtml('download_file_button');
    }
               
    
    /**
     * Retrieve HTML for the empty trashcan button
     *
     * @return string
     */
    public function getEmptyTrashcanButtonHtml()
    {
        return $this->getChildHtml('empty_trashcan_button');
    }
           
    
    /**
     * Retrieve HTML for the paste file button
     *
     * @return string
     */
    public function getPasteFileButtonHtml()
    {
        return $this->getChildHtml('paste_file_button');
    }
                   
    
    /**
     * Retrieve HTML for the compress file button
     *
     * @return HMTL string
     */
    public function getCompressFileButtonHtml()
    {
        return $this->getChildHtml('compress_file_button');
    }
                   
    
    /**
     * Retrieve HTML for the extract file button
     *
     * @return string
     */
    public function getExtractFileButtonHtml()
    {
        return $this->getChildHtml('extract_file_button');
    }
    
    
    /**
     * Retrive uploader block
     *
     * @return Mage_Adminhtml_Block_Media_Uploader
     */
    public function getUploader()
    {
        return $this->getChild('uploader');
    }
    
    
    /**
     * Retrive uploader block html
     *
     * @return string
     */
    public function getUploaderHtml()
    {
        return $this->getChildHtml('uploader');
    }
    
    
    /**
     * Check if popup was requested (from TinyMCE)
     */
    public function isStandalone()
    {
    	if(is_null($this->_standalone)) {
    		$this->_standalone = (bool)$this->getRequest()->getParam('standalone');
    	}
    	return $this->_standalone;
    }

}