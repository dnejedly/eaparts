<?php
/**
 * MageParts
 * 
 * NOTICE OF LICENSE
 * 
 * This code is copyrighted by MageParts and may not be reproduced
 * and/or redistributed without a written permission by the copyright 
 * owners. If you wish to modify and/or redistribute this file please
 * contact us at info@mageparts.com for confirmation before doing
 * so. Please note that you are free to modify this file for personal
 * use only.
 *
 * If you wish to make modifications to this file we advice you to use
 * the "local" file scope in order to aviod conflicts with future updates. 
 * For information regarding modifications see http://www.magentocommerce.com.
 *  
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF 
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY 
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * @category   MageParts
 * @package    MageParts_MediaLibrary
 * @copyright  Copyright (c) 2009 MageParts (http://www.mageparts.com/)
 * @author 	   MageParts Crew
 */

class MageParts_MediaLibrary_Helper_Validate extends Mage_Core_Helper_Abstract
{
	
	/**
	 * Files object
	 *
	 * @var MageParts_MediaLibrary_Model_Files
	 */
	protected $_filesModel;
	
	
	
	/**
	 * Validate filename extension
	 *
	 * @param string $filename [complete filename]
	 * @return boolean
	 */
	public function validateFilenameExtension($filename)
	{
		// @var MageParts_MediaLibrary_Model_Files
		$model = $this->getFilesModel();	
		
		// get file extension
		$ext = substr($filename,(strrpos($filename,'.')+1));
			
		// validate file extension
		if(!$model->isValidFileExt($ext)) {
			Mage::throwException($this->__("Invalid file extension {$ext}"));
			return false;
		}
		
		return true;
	}
	
	
	/**
     * Validates a file path, throws an exception incase the path was incorrect
     *
     * @param string $path
     * @param boolean $mustExist
     */
    public function validatePath($path, $mustExist=true)
    {
    	if(!$this->getFilesModel()->isValidPath($path,$mustExist)) {
    		Mage::throwException($this->__("Invalid file path"));
    	}
    }
    
    
    /**
     * Validates a directory path, throws an exception incase the path was incorrect
     *
     * @param string $dir
     * @param boolean $mustExist
     */
    public function validateDirPath($dir, $mustExist=true)
    {
    	if(!$this->getFilesModel()->isValidDir($dir, $mustExist)) {
    		Mage::throwException($this->__("Invalid directory path"));
    	}
    }
	
	
	/**
	 * Retrieve files model object
	 *
	 * @return MageParts_MediaLibrary_Model_Files
	 */
	public function getFilesModel()
	{
		if(is_null($this->_filesModel)) {
			$this->_filesModel = Mage::getModel('medialibrary/files');
		}
		return $this->_filesModel;
	}
	
	
	/**
	 * Validates compression method
	 * 
	 * @param string
	 */
	public function validateCompressionMethod($method)
	{
		$methods = array('zip','rar');
		
		if(!in_array($method,$methods)) {
			Mage::throwException($this->__("Invalid compression method selected."));
		}
	}
	
}