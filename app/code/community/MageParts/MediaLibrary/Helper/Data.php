<?php
/**
 * MageParts
 * 
 * NOTICE OF LICENSE
 * 
 * This code is copyrighted by MageParts and may not be reproduced
 * and/or redistributed without a written permission by the copyright 
 * owners. If you wish to modify and/or redistribute this file please
 * contact us at info@mageparts.com for confirmation before doing
 * so. Please note that you are free to modify this file for personal
 * use only.
 *
 * If you wish to make modifications to this file we advice you to use
 * the "local" file scope in order to aviod conflicts with future updates. 
 * For information regarding modifications see http://www.magentocommerce.com.
 *  
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF 
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY 
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * @category   MageParts
 * @package    MageParts_MediaLibrary
 * @copyright  Copyright (c) 2009 MageParts (http://www.mageparts.com/)
 * @author 	   MageParts Crew
 */

class MageParts_MediaLibrary_Helper_Data extends Mage_Core_Helper_Abstract
{
	
	/**
	 * Current store id
	 *
	 * @var int
	 */
	protected $_storeId;
	
	/**
	 * Files object
	 *
	 * @var MageParts_MediaLibrary_Model_Files
	 */
	protected $_filesModel;
	
	/**
	 * Request object
	 *
	 * @var unknown_type
	 */
	protected $_request;
	
	
	
	/**
	 * Get current store id
	 *
	 * @return int
	 */
	public function getStoreId()
	{
		if(is_null($this->_storeId)) {
			$this->_storeId = intval(Mage::app()->getStore()->getId());
		}
		return $this->_storeId;
	}
	
	
	/**
	 * Retrieve srcPath param value
	 *
	 * @return string
	 */
	public function getSrcPathParam()
	{
		// @var MageParts_MediaLibrary_Model_Files
		$model = $this->getFilesModel();	
	
		// get parameter value as an absolute path
		$val = $model->getAbsolutePath($this->_getRequest()->getPost('srcPath'));
		
		// check that the parameter had a value
		if(empty($val)) {
			Mage::throwException($this->__("Unable to retrieve parameter source path"));
		}
		
		// make sure that the value is a valid directory
		if(!$model->isValidDir($val)) {
			Mage::throwException(Mage::helper('medialibrary')->__("Invalid source path"));
		}
		
		return $val;
	}
	
	
	/**
	 * Retrieve destPath param value
	 *
	 * @return string
	 */
	public function getDestPathParam()
	{
		// @var MageParts_MediaLibrary_Model_Files
		$model = $this->getFilesModel();	
	
		// get parameter value as an absolute path
		$val = $model->getAbsolutePath($this->_getRequest()->getPost('destPath'));
		
		// check that the parameter had a value
		if(empty($val)) {
			Mage::throwException($this->__("Unable to retrieve parameter destination path"));
		}
		
		// make sure that the value is a valid directory
		if(!$model->isValidDir($val)) {
			Mage::throwException(Mage::helper('medialibrary')->__("Invalid destination path"));
		}
		
		return $val;
	}
	
	
	/**
	 * Retrieve srcFilename param value
	 *
	 * @return string
	 */
	public function getSrcFilenameParam()
	{
		// @var MageParts_MediaLibrary_Model_Files
		$model = $this->getFilesModel();	
	
		// get parameter value and clean the filename
		$val = $model->getValidFilename($this->_getRequest()->getPost('srcFilename'));
		
		// check that the parameter had a value
		if(empty($val)) {
			Mage::throwException($this->__("Unable to retrieve parameter source filename"));
		}
		
		return $val;
	}
	
	
	/**
	 * Retrieve destFilename param value
	 *
	 * @return string
	 */
	public function getDestFilenameParam()
	{
		// @var MageParts_MediaLibrary_Model_Files
		$model = $this->getFilesModel();	
	
		// get parameter value and clean the filename
		$val = $model->getValidFilename($this->_getRequest()->getPost('destFilename'));
		
		// check that the parameter had a value
		if(empty($val)) {
			Mage::throwException($this->__("Unable to retrieve parameter destination filename"));
		}
		
		return $val;
	}
	
	
	/**
	 * Retrieve filetype param value
	 *
	 * @return string
	 */
	public function getFileTypeParam()
	{
		// @var MageParts_MediaLibrary_Model_Files
		$model = $this->getFilesModel();	
	
		// get parameter value and clean the filename
		$val = $this->_getRequest()->getPost('filetype');
		
		// check that the parameter had a value
		if(!$model->isValidFileType($val)) {
			Mage::throwException($this->__("Invalid file type {$val}"));
		}
		
		return $val;
	}
	
	
	/**
	 * Retrieve replace param value
	 *
	 * @return string
	 */
	public function getReplaceParam()
	{
		$val = $this->_getRequest()->getPost('replace') == 'true' ? true : false;
		return $val;
	}
	
	
	/**
     * Retrieve short filename
     *
     * @param string $file
     * @return string
     */
    public function getShortFilename($file)
    {
    	$file = realpath($file);
    	
    	$ret = substr($file,strrpos($file,'/')+1);
    	$ret = empty($ret) ? 'unknown' : $ret;
    	
    	return $ret;
    }
    
    
    /**
     * Checks if a path string is secure, in other words that it doesn't include "../", "./" or similar illegal path sections 
     *
     * @param string @path
     * @return boolean
     */
    public function pathIsSecure($path)
    {
    	// path may not include "/./" or "/../"
 		if(is_int(strpos($path,'/../')) || is_int(strpos($path,'/./'))) {
 			return false;
 		}
 		
 		// path may not begin with "./" or "../"
		if((substr($path,0,2) == './') || (substr($path,0,3) == '../')) {
			return false;
		}
		
		// path may not end with "./" or "../"
		if((substr($path,strlen($path)-2) == '/.') || (substr($path,strlen($path)-3) == '/..')) {
			return false;
		}
		
		return true;
    }
	
	
	/**
	 * Retrieve files model object
	 *
	 * @return MageParts_MediaLibrary_Model_Files
	 */
	public function getFilesModel()
	{
		if(is_null($this->_filesModel)) {
			$this->_filesModel = Mage::getModel('medialibrary/files');
		}
		return $this->_filesModel;
	}
	
	
	/**
	 * Retrieve request object
	 */
	public function _getRequest()
    {
    	if(is_null($this->_request)) {
    		$this->_request = Mage::app()->getRequest();
    	}
    	return $this->_request;
    }
	
}