<?php
/**
 * MageParts
 * 
 * NOTICE OF LICENSE
 * 
 * This code is copyrighted by MageParts and may not be reproduced
 * and/or redistributed without a written permission by the copyright 
 * owners. If you wish to modify and/or redistribute this file please
 * contact us at info@mageparts.com for confirmation before doing
 * so. Please note that you are free to modify this file for personal
 * use only.
 *
 * If you wish to make modifications to this file we advice you to use
 * the "local" file scope in order to aviod conflicts with future updates. 
 * For information regarding modifications see http://www.magentocommerce.com.
 *  
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF 
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY 
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * @category   MageParts
 * @package    MageParts_MediaLibrary
 * @copyright  Copyright (c) 2009 MageParts (http://www.mageparts.com/)
 * @author 	   MageParts Crew
 */

class MageParts_MediaLibrary_Model_Files extends Mage_Core_Model_Abstract
{
	
	const MP_ML_CONF_FILTER_IMAGE 		= 'medialibrary/image/filter';
	const MP_ML_CONF_FILTER_VIDEO 		= 'medialibrary/video/filter';
	const MP_ML_CONF_FILTER_AUDIO 		= 'medialibrary/audio/filter';
	const MP_ML_CONF_FILTER_COMPRESSED 	= 'medialibrary/compressed/filter';
	const MP_ML_CONF_FILTER_ETC 		= 'medialibrary/etc/filter';
	
	const MP_ML_CONF_TC_PATH			= 'medialibrary/trashcan/path';
	const MP_ML_CONF_TMP_PATH			= 'medialibrary/general/tmp_path';
	const MP_ML_CONF_ROOT_DIRS			= 'medialibrary/general/root_directories';
	const MP_ML_CONF_CUSTOM_ROOT_DIRS	= 'medialibrary/general/custom_root_directories';
	
	/**
	 * Helper object
	 */
	protected $_helper;
	
	/**
	 * List of root directories
	 *
	 * @var array
	 */
	protected $_rootDirectories;
	
	
	/**
	 * Associative filter array
	 *
	 * @var array
	 */
	protected $_filtersAssocArr;
	
	
	/**
	 * Filters array
	 *
	 * @var array
	 */
	protected $_filtersArr;
	
	
	/**
	 * Path to the trashcan
	 *
	 * @var string
	 */
	protected $_trashcanPath;
	
	
	/**
	 * Path to the temp directory
	 *
	 * @var string
	 */
	protected $_tmpPath;
	
	
	/**
	 * Mapping icon by "type" => "class name" (type / class name = image, avi, xls, audio etc.)
	 *
	 * @var return
	 */
	protected $_iconMapping = array();
	
	
    
	/**
	 * Get icon by filetype
	 *
	 * @param string $filename
	 * @return string
	 */
    public function getIconClassByFileType( $filename )
    {
    	// get all filters as an associative array
		$filter = $this->getAllFiltersAsArray(true);
		// get file extension
		$fileExt = strtolower(substr($filename,strrpos($filename,'.')+1));
		
		if(!isset($this->_iconMapping[$fileExt])) {
			// get skin path of the catalog where the icons are located
			$skinPath = Mage::getModel('core/design_package')->getSkinBaseDir(array('_area' => 'adminhtml')) . DS . 'mageparts' . DS . 'medialibrary' . DS . 'i';
			
			foreach ($filter as $f=>$exts) {
				// check if the file extension is included in this array piece
				if(in_array($fileExt,$exts)) {
					if($f == 'etc') {
						// check for specific icon if the file type group is etc
						if(file_exists($skinPath . DS . 'file-icon-' . $fileExt . '.gif')) {
							$this->_iconMapping[$fileExt] = $fileExt;
						}
						else {
							$this->_iconMapping[$fileExt] = 'leaf';
						}
					}
					else {
						// use the file type group (image, video, compressed etc.)
						$this->_iconMapping[$fileExt] = $f;
					}
				}
			}
			
			// default icon mapping
			if(!isset($this->_iconMapping[$fileExt]) || empty($this->_iconMapping[$fileExt])) {
				$this->_iconMapping[$fileExt] = 'leaf';
			}
		}
		
		// return the classname
		return $this->_iconMapping[$fileExt];
    }

    
    /**
     * Get an array of all available filters
     *
     * @param boolean $assoc [return associatve array]
     * @return array
     */
    public function getAllFiltersAsArray( $assoc=false )
    {
    	// get store id
    	$storeId = $this->_getHelper()->getStoreId();
    	    	
    	// get an assoc version of the array
    	if($assoc) {
    		if(is_null($this->_filtersAssocArr)) {
    			$arr				= array();
				$arr['image'] 		= explode(',',Mage::getStoreConfig(self::MP_ML_CONF_FILTER_IMAGE,$storeId));
				$arr['video'] 		= explode(',',Mage::getStoreConfig(self::MP_ML_CONF_FILTER_VIDEO,$storeId));
				$arr['audio'] 		= explode(',',Mage::getStoreConfig(self::MP_ML_CONF_FILTER_AUDIO,$storeId));
				$arr['compressed'] 	= explode(',',Mage::getStoreConfig(self::MP_ML_CONF_FILTER_COMPRESSED,$storeId));		
				$arr['etc'] 		= explode(',',Mage::getStoreConfig(self::MP_ML_CONF_FILTER_ETC,$storeId));
				$this->_filtersAssocArr = $arr;
    		}
    		// return associatve array
    		return $this->_filtersAssocArr;
    	}
    	else {
    		// get a normal array containing all of the filters
    		if(is_null($this->_filtersArr)) {
	    		$arr = explode(',',Mage::getStoreConfig(self::MP_ML_CONF_FILTER_IMAGE,$storeId));
	    		$arr = array_merge($arr,explode(',',Mage::getStoreConfig(self::MP_ML_CONF_FILTER_VIDEO,$storeId)));
	    		$arr = array_merge($arr,explode(',',Mage::getStoreConfig(self::MP_ML_CONF_FILTER_AUDIO,$storeId)));
	    		$arr = array_merge($arr,explode(',',Mage::getStoreConfig(self::MP_ML_CONF_FILTER_COMPRESSED,$storeId)));
	    		$arr = array_merge($arr,explode(',',Mage::getStoreConfig(self::MP_ML_CONF_FILTER_ETC,$storeId)));
	    		$this->_filtersArr = $arr;
    		}
    		// return regular array
    		return $this->_filtersArr;
    	}
    }
    
    
    /**
     * Move a file or folder
     *
     * @param string $src [current path]
     * @param string $dest [new path]
     * @param boolean $replace [if true, replace any file with the same path as $dest]
     * @return boolean
     */
    public function move( $src, $dest, $replace ) 
    {
    	// get relpaths
    	$src = realpath($src);
  
    	// create Varien IO object
    	$io = new Varien_Io_File();
		$io->setAllowCreateFolders(true);
	
		// get helper
		$helper = $this->_getHelper();
		
		// get short filename
		$srcShortFilename = $helper->getShortFilename($src);
		
		// validate source path
		if(!$this->isValidPath($src)) {	
			Mage::throwException($helper->__("Unable to move file %s, invalid source file path.",$srcShortFilename));
		}
		
		// remove trailing slashes from destination path
		while(substr($dest,-1)=='/') {
			$dest = substr_replace($dest,'',-1);
		}
		
		// validate destination file path
		if(!$this->isValidPath($dest,false)) {
			Mage::throwException($helper->__("Unable to move file %s, invalid destination path.",$srcShortFilename));
		}
		
		// get destination directory
		$destDir = realpath(substr($dest,0,strrpos($dest,'/')));
		
		// validate destination path directory
		if(!$this->isValidDir($destDir)) {
			Mage::throwException($helper->__("Unable to move file %s, invalid destination path.",$srcShortFilename));
		}
		
		// if the dest path exists within the src path, move the new directory to a tmp location before we delete the dest path, otherwise our src path will also be deleted.
		if($replace && is_dir($src) && is_int(strpos($src,$dest))) {			
			// open current location path
            $io->open(array(
                'path' => $src
            ));
            
            // temporary destination
            $tmpDest = $destDir . DS . 'tmpMoveFile_' . time();
            
            // make sure the temp destination directory is unique, so we don't accidentally remove a directory with the same name if one already exists (unlikely since we sue time() but possible)
            while(is_dir($tmpDest)) {
            	$tmpDest = $destDir . DS . 'tmpMoveFile_' . time();
            }
            
            // validate temp destination
            if(!$this->isValidPath($tmpDest,false)) {
            	Mage::throwException($helper->__("Unable to move file %s, invalid temp destination.",$srcShortFilename));
            }
            
            // attempt to move the directory to temporary destination
            if($io->mv($src, $tmpDest)) {
            	$this->remove($dest,true);
            	$src = $tmpDest;
            }
            
            // re-validate file source path
			if(!$this->isValidPath($src)) {	
				Mage::throwException($helper->__("Unable to move file %s, invalid temporary source file path.",$srcShortFilename));
			}
		}
		
        // replace existing file / directory
        if($replace && file_exists($dest)) {
        	$this->remove($dest,true);
        }

        // open current location path
        $io->open(array(
        	'path' => is_dir($src) ? $src : $destDir
        ));
            
        // attempt to move the directory
        if($io->mv($src, $dest)) {
        	return true;
        }
		
        // failure
    	return false;
    }
      
    
    /**
     * Remove a file or folder
     *
     * @param string $file
     * @param boolean $recursive [remove subfiles / folders as well]
     * @return boolean
     */
    public function remove( $file, $recursive=false )
    {
    	// create Varien input / output file object
    	$io = new Varien_Io_File();
    
    	// get helper
		$helper = $this->_getHelper();
    
		// we need a file path
    	if(empty($file)) {
    		Mage::throwException($helper->__("Unable to delete file, no file path assigned."));
    	}
			
    	// validate file source path
    	$file = realpath($file);
    	
    	// get short filename
		$srcShortFilename = $helper->getShortFilename($file);
		
    	if(!$this->isValidPath($file)) {
    		Mage::throwException($helper->__("Invalid source file %s, please make sure that the source path of the file is accurate and try again.",$srcShortFilename));
    	}
    	
		// double-check that the file exists before deleting
		if(!file_exists($file)) {
			Mage::throwException($helper->__("Unable to delete file %s, the file does not exist.",$srcShortFilename));
		}

		// delete file or folder
		if((is_dir($file) && $io->rmdir($file,$recursive)) || (is_file($file) && $io->rm($file))) {
			// success
			return true;
		}

    	// failure
		return false;
    }
    

    /**
     * Create a new directory
     *
     * @param string $dir
     * @param int $mode
     * @param boolean $recursive [currently doesn't work, it's not necessary]
     * @return boolean
     */
    public function makeDir($dir, $mode=0777, $recursive=true)
    {
    	// get helper
		$helper = $this->_getHelper();
		
    	// create Varien input / output file object
    	$io = new Varien_Io_File();
    	$io->setAllowCreateFolders(true);
    	
    	// validate directory path
    	if(!$this->isValidPath($dir,false)) {
			Mage::throwException($helper->__("Unable to create new directory %s, invalid directory path.",$srcShortFilename));
		}
    	
    	// get destination directory path
		$path = $dir;
		
		// remove trailing slashes
		while(substr($path,-1)=='/') {
			$path = substr_replace($path,'',-1);
		}
		
		// get short filename
		$srcShortFilename = substr($path,strrpos($path,'/')+1);
		
		// directory name may not be equal to "." or "..", these are reserved by the system
		if((trim($srcShortFilename) == '.') || (trim($srcShortFilename) == '..')) {
			Mage::throwException($helper->__("Unable to create new directory %s, this directory name is reserved by the system.",$srcShortFilename));
		}
		
		// get actual destination path (one directory above the submitted directory path)
		$path = realpath(substr($path,0,strrpos($path,'/')));

		// validate path
		if(!$this->isValidPath($path)) {
			Mage::throwException($helper->__("Unable to create new directory %s, invalid destination path.",$srcShortFilename));
		}
		
    	// make sure that the requested path to be created exists within a root directory
    	if($this->isInRootDirectory($path)) {
    		// attempt to create the directory
    		if(is_dir($dir) || $io->mkdir($dir,$mode,$recursive)) {
    			return true;
    		}
    	}
    
    	return false;
    }
    

    /**
     * Copy a file or directory
     *
     * @param string $src [file to be copied]
     * @param string $dest [copy destination]
     * @param string $firstLevelDest [destination top level]
     * @return mixed
     */
    public function _copy( $src, $dest, $firstLevelDest=null )
    {	
    	// get helper
		$helper = $this->_getHelper();
		
		// get short filename
		$srcShortFilename = $helper->getShortFilename($src);
		
    	if(is_null($firstLevelDest)) {
    		$firstLevelDest = $dest;
    	}
    	
    	// results
    	$results = false;
    	
    	// create Varien input / output file object
    	$io = new Varien_Io_File();
    	
    	// validate source path
    	$src = realpath($src);
    	
    	if(!$this->isValidPath($src)) {
    		Mage::throwException($helper->__("Unable to copy %s, invalid source path.",$srcShortFilename));
    	}
		
    	if(!$this->isInRootDirectory($src)) {
			Mage::throwException($helper->__("Unable to copy %s, source path must be within a root directory.",$srcShortFilename));
		}
		
		// validate destination path
		if(!$this->isValidPath($dest,false)) {
    		Mage::throwException($helper->__("Unable to copy %s, invalid destination path.",$srcShortFilename));
		}
		
		if(!$this->isInRootDirectory($dest,false)) {
    		Mage::throwException($helper->__("Unable to copy %s, destination path must be within a root directory.",$srcShortFilename));
		}
			
		// copy directory
    	if(is_dir($src)) {
    		if($dir = opendir($src)) {
    			// create the destination directory
    			if($this->makeDir($dest)) {
    				$results = true;
    			}
    			
    			// copy files from source directory to destination directory
    			while(false !== ($file = readdir($dir))) {
    				// exclude "." & ".."
    				if(($file != '.') && ($file != '..')) {
    					// if a file was pasted within itself, skip copying the file itself. otherwise we will be stuck in an eternal copy/paste which will cramp up the hard drive
    					if(realpath($src . DS . $file)==$firstLevelDest) {
    						continue;
    					}
    					
    					// copy file
    					$this->_copy(($src . DS . $file),($dest . DS . $file),$firstLevelDest);
    				}
    			}
    		}
    		else {
    			Mage::throwException($helper->__("Unable to copy %s, unable to open source folder.",$srcShortFilename));
    		}
    	}
    	else {
	    	// make sure that the file exists
	        if (!$io->fileExists($src)) {
    			Mage::throwException($helper->__("Unable to copy %s, source file does not exist.",$srcShortFilename));
	        }
	
	        // attempt to copy the file
	        if($io->cp($src,$dest)) {
	        	$results = true;
	        }
    	}
    	
    	return $results;
    }
    
    
    /**
     * Check if a path exists in the trashcan
     *
     * @param string $path
     * @param boolean $mustExist
     * @return boolean
     */
    public function isInRootDirectory($path, $mustExist = true)
    {
    	// validate path
    	if(!$this->_getHelper()->pathIsSecure($path)) {
    		return false;
    	}
    	
    	$path = $mustExist ? realpath($path) : $path;

    	// check if the path exists
    	if($mustExist && !file_exists($path)) {
    		return false;
    	}
    	
    	// get root directories
    	$rootDirectories = $this->getRootDirectories();
    	
    	// check if the requested path is inside of a root directory
    	foreach ($rootDirectories as $r) {
    		if($this->isValidDir($r)) {
	    		$check = strpos($path,$r);
	
	    		if(is_int($check) && ($check === 0)) {
	    			// valid path
		    		return true;
		    	}
    		}
    	}

    	return false;
    }
    
    
    /**
     * Check if a path is inside of the trashcan or not
     *
     * @param string $path
     * @return boolean
     */ 
    public function isInTrashcan($path, $mustExist = true)
    {
    	// validate path
    	if(!$this->_getHelper()->pathIsSecure($path)) {
    		return false;
    	}
    	
    	$path = $mustExist ? realpath($path) : $path;

    	// check if the path exists
    	if($mustExist && !file_exists($path)) {
    		return false;
    	}
    	
    	// get trashcan path
    	$trashcanPath = $this->getTrashCanPath();
    	
    	$check = strpos($path,$trashcanPath);
	
		if(is_int($check) && ($check === 0)) {
			// valid path
    		return true;
    	}
		
		return false;
    }
    
    
    /**
     * Check if a path exists in the temp directory
     *
     * @param string $path
     * @return boolean
     */
    public function isInTempDirectory($path, $mustExist = true)
    {
    	// validate path
    	if(!$this->_getHelper()->pathIsSecure($path)) {
    		return false;
    	}
    	
    	$path = $mustExist ? realpath($path) : $path;

    	// check if the path exists
    	if($mustExist && !file_exists($path)) {
    		return false;
    	}
    	
    	// get temp path
    	$tmpPath = $this->getTmpPath();
    	
    	$check = strpos($path,$tmpPath);
	
		if(is_int($check) && ($check === 0)) {
			// valid path
    		return true;
    	}
		
		return false;
    }
    
    
	/**
	 * Retruns a list of root directories
	 *
	 * @return array
	 */
    public function getRootDirectories()
    {
    	// get store id
    	$storeId = $this->_getHelper()->getStoreId();
    	
    	// create Varien input / output file object
    	$io = new Varien_Io_File();
    	$io->setAllowCreateFolders(true);
    	
    	if(is_null($this->_rootDirectories)) {
    		$this->_rootDirectories = array();
			
    		// list of root directories
    		$rootDirs = '';
    		
    		// see if there are any custom root directories
    		if($customRootDirs = unserialize(Mage::getStoreConfig(self::MP_ML_CONF_CUSTOM_ROOT_DIRS,$storeId))) {
	    		foreach ($customRootDirs as $crd) {
	    			$rootDirs.= !empty($rootDirs) ? ",{$crd['path']}" : $crd['path'];
	    		}
    		}
    		
    		// combine the list of default and custom root directories
    		$rootDirs.= empty($rootDirs) ? Mage::getStoreConfig(self::MP_ML_CONF_ROOT_DIRS,$storeId) : ",".Mage::getStoreConfig(self::MP_ML_CONF_ROOT_DIRS,$storeId);
    			
	    	foreach (explode(',',$rootDirs) as $p) {
	    		if(trim($p)!='') {
	    			// get root dir path
		    		$dir = (Mage::getBaseDir('base') . DS . $p);
					
		    		// check if the directory path is valid
		    		if($this->isValidDir($dir,false)) {
			    		// attempt to create the root directory if it doesn't already exist
			    		if(is_dir($dir) || $io->mkdir($dir,0777,true)) {
			    			// add root directory path to the root directory list
			    			if($dirPath = realpath($dir)) {
			    				$this->_rootDirectories[] = $dirPath;
			    			}
			    		}
		    		}
		    		else {
		    			continue;
		    		}
	    		}
	    	}
    	}
    	
    	// return list of root directories
    	return $this->_rootDirectories;
    }
    
    
    /**
     * Get the trashcan path
     *
     * @return string
     */
    public function getTrashCanPath()
    {	
    	// get helper
    	$helper = $this->_getHelper();
    	
    	// get store id
    	$storeId = $this->_getHelper()->getStoreId();
    	
    	if(is_null($this->_trashcanPath)) {
	    	// create Varien input / output file object
	    	$io = new Varien_Io_File();
	    	$io->setAllowCreateFolders(true);
	    	
	    	// get trashcan path
	    	$trashcan = (Mage::getBaseDir('base') . DS . Mage::getStoreConfig(self::MP_ML_CONF_TC_PATH,$storeId));
	    	
	    	// directory path validation
    		if($this->isValidDir($trashcan,false)) {
		    	// attempt to create the trashcan path if it doesn't already exist
		    	if(is_dir($trashcan) || $io->mkdir($trashcan,0777,true)) {
		    		if($trashcanPath = realpath($trashcan)) {
		    			$this->_trashcanPath = $trashcanPath;
		    		}
		    	}
    		}
    		else {
    			Mage::throwException($helper->__("The trashcan path appears to be invalid. Stoping script execution for security reasons."));
    			return;
    		}
    	}
    	
    	// trashcan path may not be empty or null
    	if(is_null($this->_trashcanPath) || empty($this->_trashcanPath)) {
    		Mage::throwException($helper->__("Unable to retrieve trashcan path. Stoping script execution for security reasons."));
    	}
    	
    	// return trashcan path
    	return $this->_trashcanPath;
    }
    
    
    /**
     * Get path of Temp directory. Directory is used for storing temporary files, such as compressed files.
     *
     * @return string
     */
    public function getTmpPath()
    {	
    	// get helper
    	$helper = $this->_getHelper();
    	
    	// get store id
    	$storeId = $this->_getHelper()->getStoreId();
    	
    	if(is_null($this->_tmpPath)) {
	    	// create Varien input / output file object
	    	$io = new Varien_Io_File();
	    	$io->setAllowCreateFolders(true);
	    	
	    	// get temp path
	    	$tmp = (Mage::getBaseDir('base') . DS . Mage::getStoreConfig(self::MP_ML_CONF_TMP_PATH,$storeId));
	    	
	    	// directory path validation
	    	if($this->isValidDir($tmp,false)) {
	    		// attempt to create the temp path if it doesn't already exist
		    	if(is_dir($tmp) || $io->mkdir($tmp,0777,true)) {
		    		if($tmpPath = realpath($tmp)) {
		    			$this->_tmpPath = $tmpPath;
		    		}
		    	}
	    	}
	    	else {
	    		Mage::throwException($helper->__("The temp path appears to be invalid. Stoping script execution for security reasons."));
	    		return;
	    	}
    	}
    	
    	// tmp path may not be empty or null
    	if(is_null($this->_tmpPath) || empty($this->_tmpPath)) {
    		Mage::throwException($helper->__("Unable to retrieve temp path. Stoping script execution for security reasons."));
    	}
    	
    	// return temp path
    	return $this->_tmpPath;
    }
    
    
    /**
     * Compress a list of files. By default the create zip file is placed in the temp directory.
     *
     * @param array $files [list of files, with absolute path, to be compressed]
     * @param string $method [compressions method, such as 'zip' or 'rar'. Only 'zip' is available for now.]
     * @param string $zipFile [optional] - absolute path to an existing zip file where the files are to be added
     * @return string
     */
    public function compressFiles($files, $method='zip', $zipFile='')
    {
    	// get helper
    	$helper = $this->_getHelper();
	
		// compress files using zip
		if($method=='zip') {
			// make sure the zip extension has been loaded
	    	if(!extension_loaded('zip')) {
	    		Mage::throwException($helper->__("You will need to install PHP's Zip extension in order to extract and compress zip files. Ask your web hosting company to install it for you."));
	    	}
	    	
    		// create zip object
    		$zip = new ZipArchive();
    		
    		// if a zip file has not been sent along, create a new one
    		if(empty($zipFile)) {
	    		// get temp path
	    		$tmpZipPath = $this->getTmpPath() . DS . time() . '_tmp.zip';
	    		
	    		// validate zip path
	    		if(!$this->isValidPath($tmpZipPath,false)) {
	    			Mage::throwException($helper->__("Unable to create zip archive, invalid file path selected."));
	    		}
	    		
	    		// create the new zip file
	    		if($zip->open($tmpZipPath, ZIPARCHIVE::CREATE)!==TRUE) {
	    			Mage::throwException($helper->__("Unable to create zip file. Please make sure that your user permissions are properly set up"));
	    		}
    		}
    		else {
    			// get realpath of $zipFile
    			$zipFile = realpath($zipFile);
    			
    			// validate zip path
	    		if(!$this->isValidPath($zipFile)) {
	    			Mage::throwException($helper->__("Unable to open selected zip archive, invalid file path selected."));
	    		}
    			
    			// make sure that the zip file exists within one of the root directories
    			if(!$this->isInRootDirectory($zipFile)) {
    				Mage::throwException($helper->__("Invalid path to zip archive. The zip archive must exists within one of the root directories"));
    			}
    			
    			// try to open the zip file
    			if(!$zip->open($zipFile)) {
    				Mage::throwException($helper->__("Unable to open archive for writing"));
    			}
    		}
    		
    		// compress files
    		foreach ($files as $file) {
    			// get realpath of $file
    			$file = $this->getAbsolutePath($file);
    			
    			// make sure that the file exists
    			if(!file_exists($file)) {
    				Mage::throwException($helper->__("Unable to compress requested file(s), the file(s) does not exist."));
    			}
    			
    			// make sure that the file is in a root directory
    			if(!$this->isInRootDirectory($file)) {
    				Mage::throwException($helper->__("Unable to compress requested file(s). You can only compress files inside of your root directories"));
    			}
    			
    			// if the file is a directory, loop it
    			if(is_dir($file)) {
    				// get a list of all files inside of the directory
    				$fileList = $this->getFileList($file);
    				
    				// compress each of the files in the directory
    				foreach ($fileList as $f) {
    					$zip->addFile($f,substr($f,strlen(Mage::getBaseDir('base'))+1));
    				}
    			}
    			else {
    				// compress the file
    				$zip->addFile($file,substr($file,strlen(Mage::getBaseDir('base'))+1));
    			}
    		}
    		
    		// close archive
    		$zip->close();
    		
    		// retrun the path of the zip file
    		return !empty($zipFile) ? $zipFile : $tmpZipPath;
		}
		else {
			Mage::throwException($helper->__("Invalid compression method '%s' selected",$method));
		}
    	
    	// fialure
    	return false;
    }
    
    
    /**
     * Get a list of all files in a directory
     *
     * @param string $dir [absolute path]
     * @return array
     */ 
    public function getFileList($dir)
    {
    	// get helper
    	$helper = $this->_getHelper();
    	
    	// file list
    	$list = array();
    	
    	// validate directory path
    	if(!$this->isValidDir($dir)) {
    		Mage::throwException($helper->__("Unable to list directory, invalid directory path requested."));
    	}
    	
    	// make sure that the directory exists within a root directory
    	if(!$this->isInRootDirectory($dir)) {
    		Mage::throwException($helper->__("Unable to list directory, invalid directory path requested. Can only list directories within a root directory."));
    	}
    	
		// read the directory
		if($dirHandle = opendir($dir)) {
			// loop files inside of the directory
			while(false !== ($file = readdir($dirHandle))) {
				// exclude "." & ".."
				if(($file != '.') && ($file != '..')) {
					// get the realpath of the file
					$entry = realpath(($dir . DS . $file));
					
					// validate file
					if($this->isValidPath($entry)) {
						// if the file is a directory, loop it
						if(is_dir($entry)) {
							$list = array_merge($list,$this->getFileList($entry));
						}
						else {
							// add the file to $list
							$list[] = $entry;
						}
					}
				}
			}
		}
    	
    	// return file list
    	return $list;
    }
    
     
    /**
     * Extract a compressed file. Default extract location is in the compressed file's parent directory.
     *
     * @param string $file [aboslute path of the compressed file to be extracted]
     * @param string $location [optional] - extract location
     * @return boolean
     */ 
    public function extractFile($file, $location='')
    {
    	// get helper
    	$helper = $this->_getHelper();

    	// get file extension
		$ext = strtolower(substr($file,strrpos($file,'.')+1));
    	
    	// validate file
    	if(!$this->isValidPath($file)) {
    		Mage::throwException($helper->__("Unable to extract file. Invalid file path."));
    	}
    	
    	// given file path must belong to a file
    	if(!is_file($file)) {
    		Mage::throwException($helper->__("Unable to extract file."));
    	}
    	
    	// file must be in a root directory
    	if(!$this->isInRootDirectory($file)) {
			Mage::throwException($helper->__("Unable to extract file. You may only extract files within the root directory."));
		}
    	
    	// file must have valid file extension
    	Mage::helper('medialibrary/validate')->validateCompressionMethod($ext);
    	
		// get extraction location
		$location = empty($location) ? realpath(substr($file,0,strrpos($file,'/'))) : $location;
    			
		// validate location path
		if(!$this->isValidDir($location)) {
			Mage::throwException($helper->__("Unable to extract file. Invalid extraction path."));
		}
		
		// extraction path must be within a root directory
		if(!$this->isInRootDirectory($location)) {
			Mage::throwException($helper->__("Unable to extract file. Extraction path must be within a root directory."));
		}
		
		// extract zip file
		if($ext==='zip') {
			// make sure the zip extension has been loaded
	    	if(!extension_loaded('zip')) {
	    		Mage::throwException($helper->__("You will need to install PHP's ZIP extension in order to extract and compress zip files. Ask your web hosting company to install it for you."));
	    	}
	    	
	    	// create new ZipArchive object
			$zip = new ZipArchive();
			
			// attempt to extract the zip file
			if ($zip->open($file) === TRUE) {
			    $zip->extractTo($location);
			    $zip->close();
			    
			    // success
			    return true;
			}
		}
		
		// extract rar file
		else if($ext==='rar') {
			// we need the "rar" class to extract files
	    	if(!extension_loaded('rar')) {
	    		Mage::throwException($helper->__("You will need to install PHP's RAR extension in order to extract and compress rar files. Ask your web hosting company to install it for you."));
	    	}
	    	
	    	// attempt to extract the rar file
			if ($rarFile = rar_open($file)) {
				// get a list of all files in rar archive
				$list = rar_list($rarFile);
				
				// extract each file in the list
				foreach ($list as $rarEntry) {
					$entry = rar_entry_get($rarFile, $rarEntry->name);
					$entry->extract($location);
				}
				
				// close the archive
			    rar_close($rarFile);
			    
			    // success
			    return true;
			}
		}
    	
    	// fialure
    	return false;
    }
    
    
    /**
     * Retrieve helper object
     * 
     * @return MageParts_MediaLibrary_Helper_Data
     */
    public function _getHelper()
    {
    	if(is_null($this->_helper)) {
    		$this->_helper = Mage::helper('medialibrary');
    	}
    	return $this->_helper;
    }
    
    
    /**
     * Check if a directory is valid for use
     *
     * @param string $path
     * @param boolean $mustExist
     * @return boolean
     */
    public function isValidDir($dir, $mustExist=true)
    {
    	if($mustExist && !is_dir($dir)) {
    		return false;
    	}
    	
    	return $this->isValidPath($dir,$mustExist);
    }
    
    
    /**
     * Check if a directory is _really_ valid, this is very important to ensure security
     *
     * @param string $path
     * @param boolean $mustExist
     * @return boolean
     */
    public function isValidPath($path, $mustExist=true)
    {
    	// validate path
    	if(!$this->_getHelper()->pathIsSecure($path)) {
    		return false;
    	}
    	
    	// get realpath
    	$path = $mustExist ? realpath($path) : $path;

    	// check if the path exists
    	if($mustExist && !file_exists($path)) {
    		return false;
    	}
    	
    	// valid base directories
    	$validMagentoDirs = array(
    		realpath(Mage::getBaseDir('base') . DS . 'media'),
    		realpath(Mage::getBaseDir('base') . DS . 'skin')
    	);
    	
    	// check if the path resides within one of the valid base directories
    	foreach ($validMagentoDirs as $vbd) {
    		$check = strpos($path,$vbd);

    		if(is_int($check) && ($check === 0)) {
    			// valid path
	    		return true;
	    	}
    	}
    	
    	// invalid path
    	return false;
    }
    
    
    /**
     * Get a directory path
     *
     * @param string $path
     * @return string
     */
    public function getAbsolutePath($path)
    {
    	$ret = realpath(Mage::getBaseDir('base') . DS .$path);
    	return $ret;
    }
    
    
    /**
     * Get relative path
     *
     * @param string $path
     * @return string
     */
    public function getRelativePath($path)
    {
    	// get realpath
    	$path = realpath($path);
    	
    	// validate file path
   		if($path && $this->isValidPath($path)) {
   			$ret = str_replace(realpath(Mage::getBaseDir('base')),'',$path);
   			$ret = (strpos($ret,'/')===0) ? substr($ret,1) : $ret;
   			
   			return $ret;
   		}
   		return;
    }
    
    
    /**
     * Validate file type
     *
     * @return boolean
     */
    public function isValidFileType($type)
    {
    	$types = $this->getAllFiltersAsArray(true);
    	if(key_exists($type,$types) || $type == 'folder') {
    		return true;
    	}
    	return false;
    }
    
    
    /**
     * Validate file ext
     *
     * @return boolean
     */
    public function isValidFileExt($ext)
    {
    	$exts = $this->getAllFiltersAsArray();
    	if(in_array($ext,$exts)) {
    		return true;
    	}
    	return false;
    }
    
    
    /**
     * Clean and return filename
     *
     * @param string $filename
     * @return string
     */
    public function getValidFilename($filename)
    {
    	$_regex = "/[^a-zA-Z0-9\.\_\-]/isU";
    	return preg_replace($_regex,'',$filename);
    }
    
}