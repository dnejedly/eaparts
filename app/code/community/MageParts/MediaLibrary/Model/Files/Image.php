<?php
/**
 * MageParts
 * 
 * NOTICE OF LICENSE
 * 
 * This code is copyrighted by MageParts and may not be reproduced
 * and/or redistributed without a written permission by the copyright 
 * owners. If you wish to modify and/or redistribute this file please
 * contact us at info@mageparts.com for confirmation before doing
 * so. Please note that you are free to modify this file for personal
 * use only.
 *
 * If you wish to make modifications to this file we advice you to use
 * the "local" file scope in order to aviod conflicts with future updates. 
 * For information regarding modifications see http://www.magentocommerce.com.
 *  
 * DISCLAIMER
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" 
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE 
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE 
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE 
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES 
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF 
 * USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY 
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE 
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED 
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * @category   MageParts
 * @package    MageParts_MediaLibrary
 * @copyright  Copyright (c) 2009 MageParts (http://www.mageparts.com/)
 * @author 	   MageParts Crew
 */

class MageParts_MediaLibrary_Model_Files_Image extends Mage_Core_Model_Abstract
{
	
	/**
	 * Resize an image
	 *
	 * @param string $file
	 * @param int $w
	 * @param int $h
	 * @return boolean
	 */
	public function resize($file, $w, $h)
	{
		$w = intval($w);
		$h = intval($h);
		
		// @var MageParts_MediaLibrary_Model_Files
		$model = Mage::getModel('medialibrary/files');
		
		// @var MageParts_MediaLibrary_Helper_Data
		$helper = Mage::helper('medialibrary');
		
		// validate file path
		if(!$model->isValidPath($file)) {
			Mage::throwException($helper->__("Unable to resize file, invalid file path"));
			return false;
		}
		
		// get current image dimensions
		$imageSize = getimagesize($file);

		// default values for $w & $h
		if(empty($w)) {
			$w = $imageSize[0];
		}
		
		if(empty($h)) {
			$h = $imageSize[1];
		}
		
		// if the image dimensions has changed, resize the image
		if($w != $imageSize[0] || $h != $imageSize[1]) {
			$processor = new Varien_Image($file);
			$processor->resize($w,$h);
			$processor->keepAspectRatio(false);
	    	$processor->save($file);
		}
		
		return true;
	}
    
}