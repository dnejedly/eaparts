<?php
class Hidden_Filter_IndexController extends Mage_Core_Controller_Front_Action{
    public function IndexAction() {
      $session = Mage::getSingleton("customer/session");
       
      //$coreSession = Mage::getSingleton("core/session");
		//print_r($this->getRequest()->getParams());
                $make = $this->getRequest()->getParam('make');
                
                if($make!=""){  
                   
                    $session->setData("sesMake",$make);
                     $attrValue=$this->attributevalueAction($make);
                     $make=$attrValue['value'];
                    
                }else{ 
                    
                    $make =$session->getData('sesMake');
                    $attrValue=$this->attributevalueAction($make);
                    $make=$attrValue['value'];
                }
                
                $model = $this->getRequest()->getParam('model');
                if($model!=""){
                    $session->setData("sesModel",$model);
                    $attrValue=$this->attributevalueAction($model);
                     $model=$attrValue['value'];
                    
                }else{ 
                    $model =$session->getData('sesModel');
                    $attrValue=$this->attributevalueAction($model);
                     $model=$attrValue['value'];
                }
               
                $year = $this->getRequest()->getParam('year');
                if($year!=""){
                    $session->setData("sesYear",$year);
                    $attrValue=$this->attributevalueAction($year);
                     $year=$attrValue['value'];
                    
                }else{ 
                    $year =$session->getData('sesYear');
                    $attrValue=$this->attributevalueAction($year);
                     $year=$attrValue['value'];
                }
                 
                $engineno=$this->getRequest()->getParam('engineno');
                if($engineno!=""){
                    $session->setData("sesEngineno",$engineno);
                    $attrValue=$this->attributevalueAction($engineno);
                     $engineno=$attrValue['value'];
                    
                }else{ 
                    $engineno =$session->getData('sesEngineno');
                    $attrValue=$this->attributevalueAction($engineno);
                     $engineno=$attrValue['value'];
                }
                $catId=$this->getRequest()->getParam('catId');
                
	  $this->loadLayout();   
	  $this->getLayout()->getBlock("head")->setTitle($this->__("Select Vehicle"));
	        $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
      $breadcrumbs->addCrumb("home", array(
                "label" => $this->__("Home Page"),
                "title" => $this->__("Home Page"),
                "link"  => Mage::getBaseUrl()
		   ));
                /*$make=$session->getData("sesMake");
                $model=$session->getData("sesModel");
                $year=$session->getData("sesYear");
                $engineno=$session->getData("sesEngineno");*/
      
                if($make=='' && $model=='' && $year=='' && $engineno==''){
                    $filter='Select Vehicle';
                    
                }else{ 
                    if($make==''){$make='';}
                    if($model==''){$model='';}
                    if($year==''){$year='';}
                    if($engineno==''){$engineno='';}
                    
                    $filter=$make.' '.$model.' '.$engineno.' '.$year;
                     $breadcrumbs->addCrumb("Select Vehicle", array(
                "label" => $this->__($filter),
                "title" => $this->__("Select Vehicle"),
                "link"  => Mage::getBaseUrl().'filter/index/index/'
		   ));
                }
                    
               
                $categoryArr=Mage::getModel('catalog/category')->load($catId);
                $catName=$categoryArr->getName();
                $breadcrumbs->addCrumb("Category", array(
                "label" => $this->__($catName),
                "title" => $this->__($catName)
		   ));

      $this->renderLayout(); 
	  
    }
    public function attributevalueAction($id)
    {
        
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $select ="select value from eav_attribute_option_value where option_id='".$id."'";
        $attrValue =$connection->fetchRow($select);
        return $attrValue;
    }
    public function modelAction()
    {
            $data_array=array();
            $make = $this->getRequest()->getParam('make');
            $model = $this->getRequest()->getParam('model');
            $engineno = $this->getRequest()->getParam('engineno');
            $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
            $select ="select * from filter where make='".$make."'";

            if($model!='0'){
             //   $data_array[''] = 'Select Engine';
                $select .="AND model='".$model."'";
            }
            if($engineno  != '0'){
               // $data_array[''] = 'Select Engine';
                $select .="AND engineno='".$engineno."'";
            }
            $rowArray =$connection->fetchAll($select);

           // $data_array[''] = "Select Model";

            if(count($rowArray)>0)
            {
                foreach ($rowArray as $product) {
                    $model=$product['model'];
                        if($model!="" && $model!="0"){
                        $modelValue=  $this->attributevalueAction($model);
                        $data_array[$model] = $modelValue['value'];
                        }
                }

            }
            ksort($data_array);
            echo json_encode($data_array);
            exit;

    }

    public function enginenoAction()
    {
            $data_array=array();
            $make = $this->getRequest()->getParam('make');
            $model = $this->getRequest()->getParam('model');
            $engineno = $this->getRequest()->getParam('engineno');
            $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
            $select ="select * from filter where make='".$make."'";

            if($model!='0'){
            //    $data_array[''] = 'Select Year';
                $select .="AND model='".$model."'";
            }
            if($engineno  != '0'){
              //  $data_array[''] = 'Select Engine';
                $select .="AND engineno='".$engineno."'";
            }
            $rowArray =$connection->fetchAll($select);
            
            if(count($rowArray)>0)
            {
                foreach ($rowArray as $product) {
                    $engineno=$product['engineno'];
                        if($engineno!="" && $engineno!="0"){
                            $enginenoValue=  $this->attributevalueAction($engineno);
                            $data_array[$engineno] = $enginenoValue['value'];
                        }
                }

            }
            ksort($data_array);
            echo json_encode($data_array);
            exit;

    }
    public function yearAction()
    {
            $data_array=array();
            $make = $this->getRequest()->getParam('make');
            $model = $this->getRequest()->getParam('model');
            $engineno = $this->getRequest()->getParam('engineno');
            $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
            $select ="select * from filter where make='".$make."'";

            if($model!='0'){
              //  $data_array[''] = 'Select Year';
                $select .="AND model='".$model."'";
            }
            if($engineno  != '0'){
                //$data_array[''] = 'Select Engine';
                $select .="AND engineno='".$engineno."'";
            }
            $rowArray =$connection->fetchAll($select);

           // $data_array[''] = "Select Year";
            if(count($rowArray)>0)
            {
                foreach ($rowArray as $product) {
                    $year=$product['year'];
                    if($year!="" && $year!="0"){
                        $yearValue=  $this->attributevalueAction($year);
                        $data_array[$year] = $yearValue['value'];
                    }
                }

            }
            ksort($data_array);
            echo json_encode($data_array);
            exit;

    }
    /*public function modelAction()
    {
        
            $data_array=array();
            //print_r($this->getRequest()->getParam());exit;
            $make = $this->getRequest()->getParam('make');
            $model = $this->getRequest()->getParam('model');
            $year = $this->getRequest()->getParam('year');
            $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
            $select ="select * from filter where make='".$make."'";
                if($make!='-1'){
                   // $data_array[-1] = 'Select ';
                } else if($model!='-1'){
                   // $data_array[-1] = 'Select Year';
                    $select .="AND model='".$model."'";
                } else if($year  != '-1'){
                    $data_array[-1] = 'Select Engine No';
                    $select .="AND year='".$year."'";
                }else if($make == '-1'){  // $data_array[-1] = 'Select Model'; 
                    }
                    else if($model == '-1'){  // $data_array[-1] = 'Select Year';
                        }
                else if($year == '-1'){   //$data_array[-1] = 'Select Engine No';
                    }
                
                $rowArray =$connection->fetchAll($select);

            foreach ($rowArray as $product) {
                if($make != '-1'){
                $modelValue=$product['model'];}
                if($model  != '-1'){
                $modelValue=$product['year'];}
                if($year  != '-1'){
                $modelValue=$product['engineno'];}
            $data_array[$modelValue] = $modelValue;
        }

            //  $as_response = array('id'=>'size','id1'=>'size1');
            echo json_encode($data_array);
            exit;
    }*/
    
}
