<?php
class Hidden_Filter_Block_Adminhtml_Filter_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
		public function __construct()
		{
				parent::__construct();
				$this->setId("filter_tabs");
				$this->setDestElementId("edit_form");
				$this->setTitle(Mage::helper("filter")->__("Vehicle selector information"));
		}
		protected function _beforeToHtml()
		{
				$this->addTab("form_section", array(
				"label" => Mage::helper("filter")->__("Vehicle selector information"),
				"title" => Mage::helper("filter")->__("Vehicle  selector information"),
				"content" => $this->getLayout()->createBlock("filter/adminhtml_filter_edit_tab_form")->toHtml(),
				));
				return parent::_beforeToHtml();
		}

}
