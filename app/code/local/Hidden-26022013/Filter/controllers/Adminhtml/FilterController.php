<?php

class Hidden_Filter_Adminhtml_FilterController extends Mage_Adminhtml_Controller_Action
{
		protected function _initAction()
		{
				$this->loadLayout()->_setActiveMenu("catalog/filter")->_addBreadcrumb(Mage::helper("adminhtml")->__("Filter  Manager"),Mage::helper("adminhtml")->__("Filter Manager"));
				return $this;
		}
		public function indexAction() 
		{
			    $this->_title($this->__("Filter"));
			    $this->_title($this->__("Manager Vehicle Selector"));

				$this->_initAction();
				$this->renderLayout();
		}
		public function editAction()
		{			    
                $this->_title($this->__("Filter"));
				$this->_title($this->__("Filter"));
			    $this->_title($this->__("Edit Vehicle"));
				
				$id = $this->getRequest()->getParam("id");
				$model = Mage::getModel("filter/filter")->load($id);
				if ($model->getId()) {
					Mage::register("filter_data", $model);
					$this->loadLayout();
					
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Filter Manager"), Mage::helper("adminhtml")->__("Filter Manager"));
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Filter Description"), Mage::helper("adminhtml")->__("Filter Description"));
					$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
                                        $this->loadLayout()->_setActiveMenu("catalog/filter");
					$this->_addContent($this->getLayout()->createBlock("filter/adminhtml_filter_edit"))->_addLeft($this->getLayout()->createBlock("filter/adminhtml_filter_edit_tabs"));
					$this->renderLayout();
				} 
				else {
					Mage::getSingleton("adminhtml/session")->addError(Mage::helper("filter")->__("Item does not exist."));
					$this->_redirect("*/*/");
				}
		}
                public function editfilterAction()
		{	
                    //print_r($this->getRequest()->getParam("id"));exit;
                    
                            $this->_title($this->__("Filter"));
				$this->_title($this->__("Filter"));
			    $this->_title($this->__("Edit Vehicle"));
				
				$id = $this->getRequest()->getParam("pid");
                                $model = Mage::getModel("filter/filter")->getCollection();
                                $model->addFilter('sku',$id );
                                $filterArr=$model->getData();
                                //echo "<pre>";print_r($filterArr[0]['filter_id']);
				//$model = Mage::getModel("filter/filter")->load($id);
                                
                                
                               
				if ($filterArr[0]['filter_id']) {
                                        $model = Mage::getModel("filter/filter")->load($filterArr[0]['filter_id']);
					Mage::register("filter_data", $model);
					$this->loadLayout();
					
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Filter Manager"), Mage::helper("adminhtml")->__("Filter Manager"));
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Filter Description"), Mage::helper("adminhtml")->__("Filter Description"));
					$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
                                        $this->loadLayout()->_setActiveMenu("catalog/filter");
					$this->_addContent($this->getLayout()->createBlock("filter/adminhtml_filter_edit"))->_addLeft($this->getLayout()->createBlock("filter/adminhtml_filter_edit_tabs"));
					$this->renderLayout();
				} 
				else {
					$this->newAction();

                                    }
		}

		public function newAction()
		{

		$this->_title($this->__("Filter"));
		$this->_title($this->__("Filter"));
		$this->_title($this->__("New Item"));

        $id   = $this->getRequest()->getParam("id");
		$model  = Mage::getModel("filter/filter")->load($id);

		$data = Mage::getSingleton("adminhtml/session")->getFormData(true);
		if (!empty($data)) {
			$model->setData($data);
		}

		Mage::register("filter_data", $model);

		$this->loadLayout();
		$this->_setActiveMenu("catalog/filter");

		$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Filter Manager"), Mage::helper("adminhtml")->__("Filter Manager"));
		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Filter Description"), Mage::helper("adminhtml")->__("Filter Description"));


		$this->_addContent($this->getLayout()->createBlock("filter/adminhtml_filter_edit"))->_addLeft($this->getLayout()->createBlock("filter/adminhtml_filter_edit_tabs"));

		$this->renderLayout();

		}
                function getLastInsertId($tableName, $primaryKey)
{
        //SELECT MAX(id) FROM table
        $db = Mage::getModel('core/resource')->getConnection('core_read');
        $result = $db->raw_fetchRow("SELECT MAX(`{$primaryKey}`) as LastID FROM `{$tableName}`");
        return $result['LastID'];
}
		public function saveAction()
		{

			$post_data=$this->getRequest()->getPost();


				if ($post_data) {
                                    

					try {
                                            
                                                    $product = Mage::getModel('catalog/product');
                                                    $id = $post_data['sku'];
                                                    $product->load($id);
                                                    if($post_data['make-text']==""){
                                                        if($post_data['make']=="-1"){
                                                            Mage::getSingleton("adminhtml/session")->addError('Please select Make.');
                                                            $this->_redirect("*/*/edit");
                                                            return;
                                                        }
                                                        $post_data['make']=$post_data['make'];
                                                    }else{
                                                        $post_data['make']=$post_data['make-text'];
                                                    }

                                                    if($post_data['model-text']==""){
                                                        if($post_data['model']=="-1"){
                                                            Mage::getSingleton("adminhtml/session")->addError('Please select Model.');
                                                            $this->_redirect("*/*/edit");
                                                            return;
                                                        }
                                                        $post_data['model']=$post_data['model'];
                                                    }else{
                                                        $post_data['model']=$post_data['model-text'];
                                                    }
                                                    if($post_data['year-text']==""){
                                                        if($post_data['year']=="-1"){
                                                            Mage::getSingleton("adminhtml/session")->addError('Please select Year.');
                                                            $this->_redirect("*/*/edit");
                                                            return;
                                                        }
                                                        $post_data['year']=$post_data['year'];
                                                    }else{
                                                        $post_data['year']=$post_data['year-text'];
                                                    }
                                                    if($post_data['engineno-text']==""){
                                                        if($post_data['engineno']=="-1"){
                                                            Mage::getSingleton("adminhtml/session")->addError('Please select Engine No.');
                                                            $this->_redirect("*/*/edit");
                                                            return;
                                                        }
                                                        $post_data['engineno']=$post_data['engineno'];
                                                    }else{
                                                        $post_data['engineno']=$post_data['engineno-text'];
                                                    }

                                                    /*$connection = Mage::getSingleton('core/resource')->getConnection('core_read');

                                                    $select ="select COUNT(*) AS total from filter where sku='".$post_data['sku']."' and make='".$post_data['make']."' and model='".$post_data['model']."' and year='".$post_data['year']."' and engineno='".$post_data['engineno']."'";

                                                    $rowArray =$connection->fetchRow($select);*/

                                                    //print_r($post_data);exit;

                                                    $lastInsertId="";
                                                    $argArr = array('make','model','year','engineno');
                                                    for($i=0;$i<count($argArr);$i++){

                                                        //$arg_attribute = 'make';
                                                        $arg_attribute=$argArr[$i];
                                                        $arg_value = $post_data[$arg_attribute];
                                                        $attr_model = Mage::getModel('catalog/resource_eav_attribute');
                                                        $attr = $attr_model->loadByCode('catalog_product', $arg_attribute);
                                                        $attr_id = $attr->getAttributeId();
                                                       $attribute = Mage::getSingleton('eav/config')->getAttribute('catalog_product', $argArr[$i]);
                                                        if ($attribute->usesSource()) {
                                                            $options = $attribute->getSource()->getAllOptions(false);
                                                            $cnt=0;
                                                            for($j=0;$j<count($options);$j++){
                                                                if($options[$j]['label']==$arg_value){
                                                                    $cnt++;
                                                                    $lastInsertId=$options[$j]['value'];
                                                                }
                                                                    
                                                            }
                                                        }
                                                        $setup = new Mage_Eav_Model_Entity_Setup('core_setup');
                                                       
                                                        if($cnt==0){
                                                        $option = array();
                                                        $option['attribute_id'] = $attr_id;
                                                        $option['value'][$arg_attribute.'_option'][0] = $arg_value;
                                                        $setup->addAttributeOption($option);
                                                        }
                                                       
                                                        $optionTable = $setup->getTable('eav/attribute_option');
                                                        if($lastInsertId == ""){
                                                            $lastInsertId = $this->getLastInsertId($optionTable, 'option_id');
                                                        
                                                        }
                                                     //   echo "<pre>";print_r($lastInsertId);
                                                        $product->addAttributeUpdate($argArr[$i],$lastInsertId,0);
                                                        $product->save();
                                                        $lastInsertId='';

                                                    }
                                                    //print_r($post_data);exit;

                                                    $model = Mage::getModel("filter/filter")
                                                    ->addData($post_data)
                                                    ->setId($this->getRequest()->getParam("id"))
                                                    ->save();

                                                    Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Filter was successfully saved"));
                                                    Mage::getSingleton("adminhtml/session")->setFilterData(false);

                                                    if ($this->getRequest()->getParam("back")) {
                                                            $this->_redirect("*/*/edit", array("id" => $model->getId()));
                                                            return;
                                                    }
                                                    $this->_redirect("*/*/");
                                                    return;
                                               
					} 
					catch (Exception $e) { 
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						Mage::getSingleton("adminhtml/session")->setFilterData($this->getRequest()->getPost());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					return;
					}

				}
				$this->_redirect("*/*/");
		}



		public function deleteAction()
		{
				if( $this->getRequest()->getParam("id") > 0 ) {
					try {
						$model = Mage::getModel("filter/filter");
						$model->setId($this->getRequest()->getParam("id"))->delete();
						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
						$this->_redirect("*/*/");
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					}
				}
				$this->_redirect("*/*/");
		}

		
		public function massRemoveAction()
		{
			try {
				$ids = $this->getRequest()->getPost('filter_ids', array());
				foreach ($ids as $id) {
                      $model = Mage::getModel("filter/filter");
					  $model->setId($id)->delete();
				}
				Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item(s) was successfully removed"));
			}
			catch (Exception $e) {
				Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
			}
			$this->_redirect('*/*/');
		}
                public function modelAction()
		{
                        $data_array=array();
                        $make = $this->getRequest()->getParam('make');
                        $model = $this->getRequest()->getParam('model');
                        $year = $this->getRequest()->getParam('year');
                        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
                        $select ="select * from filter where make='".$make."'";
                            if($make!='-1'){
                                $data_array[-1] = 'Select Model';
                            }
                            else{   $data_array[-1] = 'Select Model';}
                            if($model!='-1'){
                                $data_array[-1] = 'Select Year';
                                $select .="AND model='".$model."'";
                            }
                            if($year  != '-1'){
                                $data_array[-1] = 'Select Engine No';
                                $select .="AND year='".$year."'";
                            }
                          $rowArray =$connection->fetchAll($select);
                        
                        foreach ($rowArray as $product) {
                            if($make != '-1'){
                            $modelValue=$product['model'];}
                            if($model  != '-1'){
                            $modelValue=$product['year'];}
                            if($year  != '-1'){
                            $modelValue=$product['engineno'];}
                        $data_array[$modelValue] = $modelValue;
                    }
                        
                      //  $as_response = array('id'=>'size','id1'=>'size1');
                        echo json_encode($data_array);
                        exit;
		}
			
}
