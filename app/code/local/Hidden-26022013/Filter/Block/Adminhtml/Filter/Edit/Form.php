<?php
class Hidden_Filter_Block_Adminhtml_Filter_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{
				$form = new Varien_Data_Form(array(
				"id" => "edit_form",
				"action" => $this->getUrl("*/*/save", array("id" => $this->getRequest()->getParam("id"))),
				"method" => "post",
				"enctype" =>"multipart/form-data",
				)
				);
				$form->setUseContainer(true);
				$this->setForm($form);
				return parent::_prepareForm();
		}
}
?>
<script>
    
    
    function showhide(id){
        
        var make = document.getElementById(id);
        make.style.display = "block";
        
    }
   

function getModelList(obj){ 
        var reloadurl = '<?php echo Mage::helper('adminhtml')->getUrl('filter/adminhtml_filter/model') ?>';
        
	var param=  'make='+obj.value+'&model=-1&year=-1';
        if(obj.value!='-1'){
	new Ajax.Request(reloadurl, {
	method: 'post',
	parameters: param,
	onComplete: function(transport) {
            $('model').innerHTML = "";
            response = transport.responseText;
            var select = document.createElement("select");
            var obj = JSON.parse(response);
            //alert(obj.toSource())
            var combobox = "";
            var ob;
            for(ob in obj){
            if(ob=='-1'){
                 $('year').innerHTML = '<option value="-1">Select Year</option>';
                 $('engineno').innerHTML = '<option value="-1">Select Engine No</option>';
            }    
                    combobox =  combobox +'<option value= "'+ob+'">';
                    combobox =  combobox + obj[ob];
                    combobox =  combobox +'</option>';
                    
            }
            
            $('model').innerHTML = combobox;
        }
	});
        }
}
function getYearList(obj){ 
        var reloadurl = '<?php echo Mage::helper('adminhtml')->getUrl('filter/adminhtml_filter/model') ?>';
        
	var param=  'make='+$('make').value+'&model='+obj.value+'&year=-1';
       if(obj.value!='-1'){
	new Ajax.Request(reloadurl, {
	method: 'post',
	parameters: param,
	onComplete: function(transport) {
            $('year').innerHTML = "";
            response = transport.responseText;
            var select = document.createElement("select");
            var obj = JSON.parse(response);
            var combobox = "";
            var ob;
            //alert(obj.toSource())
            for(ob in obj){
            if(ob=='-1'){
                $('engineno').innerHTML = '<option value="-1">Select Engine No</option>';
            }
                    combobox =  combobox +'<option value= "'+ob+'">';
                    combobox =  combobox + obj[ob];
                    combobox =  combobox +'</option>';
            }
            $('year').innerHTML = combobox;
        }
	});}
}
function getEngnolList(obj){ 
        var reloadurl = '<?php echo Mage::helper('adminhtml')->getUrl('filter/adminhtml_filter/model') ?>';
        
	var param=  'make='+$('make').value+'&model='+$('model').value+'&year='+obj.value;
        if(obj.value!='-1'){
	new Ajax.Request(reloadurl, {
	method: 'post',
	parameters: param,
	onComplete: function(transport) {
            $('engineno').innerHTML = "";
            response = transport.responseText;
            var select = document.createElement("select");
            var obj = JSON.parse(response);
            var combobox = "";
            var ob;
            for(ob in obj){
                    combobox =  combobox +'<option value= "'+ob+'">';
                    combobox =  combobox + obj[ob];
                    combobox =  combobox +'</option>';
            }
            $('engineno').innerHTML = combobox;
        }
	});
        }
}
</script>
