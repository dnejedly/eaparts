<?php
	
class Hidden_Filter_Block_Adminhtml_Filter_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
		public function __construct()
		{

				parent::__construct();
				$this->_objectId = "filter_id";
				$this->_blockGroup = "filter";
				$this->_controller = "adminhtml_filter";
				$this->_updateButton("save", "label", Mage::helper("filter")->__("Save Vehicle Selector"));
				$this->_updateButton("delete", "label", Mage::helper("filter")->__("Delete Vehicle Selector"));

				$this->_addButton("saveandcontinue", array(
					"label"     => Mage::helper("filter")->__("Save And Continue Edit"),
					"onclick"   => "saveAndContinueEdit()",
					"class"     => "save",
				), -100);



				$this->_formScripts[] = "
							var editForm = new varienForm('edit_form');
							Validation.addAllThese(
[
['validate-make', 'Please Select Make', function(v) {
if(document.getElementById('make').value == '-1' && document.getElementById('make-text').value == '')
{
return false;
}
else if(document.getElementById('make-text').value != '')
{
return true;
}
else if(document.getElementById('make-text').value != '-1')
{
return true;
}
else
{
return false;
}
}],
['validate-model', 'Please Select Model', function(v) {
if(document.getElementById('model').value == '-1' && document.getElementById('model-text').value == '')
{
return false;
}
else if(document.getElementById('model-text').value != '')
{
return true;
}
else if(document.getElementById('model-text').value != '-1')
{
return true;
}
else
{
return false;
}
}],
['validate-year', 'Please Select Year', function(v) {
if(document.getElementById('year').value == '-1' && document.getElementById('year-text').value == '')
{
return false;
}
else if(document.getElementById('year-text').value != '')
{
return true;
}
else if(document.getElementById('year-text').value != '-1')
{
return true;
}
else
{
return false;
}
}]  ,
['validate-engineno', 'Please Select Engine', function(v) {
if(document.getElementById('engineno').value == '-1' && document.getElementById('engineno-text').value == '')
{
return false;
}
else if(document.getElementById('engineno-text').value != '')
{
return true;
}
else if(document.getElementById('engineno-text').value != '-1')
{
return true;
}
else
{
return false;
}
}]    
])
							
							
							
							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
						";
		}

		public function getHeaderText()
		{
				if( Mage::registry("filter_data") && Mage::registry("filter_data")->getId() ){

				    return Mage::helper("filter")->__("Edit/Add Vehicle selector (ID:'%s')", $this->htmlEscape(Mage::registry("filter_data")->getId()));

				} 
				else{

				     return Mage::helper("filter")->__("Add Vehicle Selector");

				}
		}
}

