<?php


class Hidden_Filter_Block_Adminhtml_Filter extends Mage_Adminhtml_Block_Widget_Grid_Container{

	public function __construct()
	{

	$this->_controller = "adminhtml_filter";
	$this->_blockGroup = "filter";
	$this->_headerText = Mage::helper("filter")->__("Manage Vehicle Selector");
	$this->_addButtonLabel = Mage::helper("filter")->__("Add Vehicle Selector");
	parent::__construct();
	
	}

}