<?php
class Hidden_Filter_Block_Adminhtml_Filter_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{
				$form = new Varien_Data_Form(array(
				"id" => "edit_form",
				"action" => $this->getUrl("*/*/save", array("id" => $this->getRequest()->getParam("id"))),
				"method" => "post",
				"enctype" =>"multipart/form-data",
				)
				);
				$form->setUseContainer(true);
				$this->setForm($form);
				return parent::_prepareForm();
		}
}
?>
<script>
    function showhide(id){
        
        var make = document.getElementById(id);
        make.style.display = "block";
        
    }
   

function getModelList(val,model){ 
        var reloadurl = '<?php echo Mage::helper('adminhtml')->getUrl('filter/adminhtml_filter/model') ?>';
        var param=  'make='+val+'&model=0&engineno=0';
        if(val!='0'){
	new Ajax.Request(reloadurl, {
	method: 'post',
	parameters: param,
	onComplete: function(transport) {
            $('model').innerHTML = "";
            response = transport.responseText;
            var select = document.createElement("select");
            var obj = JSON.parse(response);
            //alert(obj.toSource())
            var combobox = "";
            var ob;
            for(ob in obj){
            if(ob==''){
                // $('year').innerHTML = '<option value="0">Select Year</option>';
                // $('engineno').innerHTML = '<option value="0">Select Engine</option>';
            }    
                var sel = '';
                if(model == ob){
                        sel = 'selected="selected"';
                }

                combobox =  combobox +'<option value= "'+ob+'" '+sel+'>';
                combobox =  combobox + obj[ob];
                combobox =  combobox +'</option>';
                    
            }
            
            $('model').innerHTML = combobox;
             if(model != ''){
                 engineno=document.getElementById('engineno').value;                 
                getEngnoList(model,engineno);
             }
         }
	});
        }
}
function getEngnoList(val,engineno){ 
        var reloadurl = '<?php echo Mage::helper('adminhtml')->getUrl('filter/adminhtml_filter/engineno') ?>';
        
	var param=  'make='+$('make').value+'&model='+val+'&engineno=0';
       if(val!='0'){
	new Ajax.Request(reloadurl, {
	method: 'post',
	parameters: param,
	onComplete: function(transport) {
            $('engineno').innerHTML = "";
            response = transport.responseText;
            var select = document.createElement("select");
            var obj = JSON.parse(response);
            var combobox = "";
            var ob;
            //alert(obj.toSource())
            for(ob in obj){
            if(ob==''){
              //  $('year').innerHTML = '<option value="0">Select Year</option>';
            }
                var sel = '';
                if(engineno == ob){
                        sel = 'selected="selected"';
                }

                combobox =  combobox +'<option value= "'+ob+'" '+sel+'>';
                combobox =  combobox + obj[ob];
                combobox =  combobox +'</option>';
            }
            $('engineno').innerHTML = combobox;
            if(engineno != ''){
                year=document.getElementById('year').value;                 
                getYearList(engineno,year);
            }
        }
	});}
}
function getYearList(val,year){ 

        var reloadurl = '<?php echo Mage::helper('adminhtml')->getUrl('filter/adminhtml_filter/year') ?>';
        
	var param=  'make='+$('make').value+'&model='+$('model').value+'&engineno='+val;
        if(val!='0'){
	new Ajax.Request(reloadurl, {
	method: 'post',
	parameters: param,
	onComplete: function(transport) {
            $('year').innerHTML = "";
            response = transport.responseText;
            var select = document.createElement("select");
            var obj = JSON.parse(response);
            var combobox = "";
            var ob;
            for(ob in obj){
                var sel = '';
                if(year == ob){
                        sel = 'selected="selected"';
                }
                combobox =  combobox +'<option value= "'+ob+'" '+sel+'>';
                combobox =  combobox + obj[ob];
                combobox =  combobox +'</option>';
            }
            $('year').innerHTML = combobox;
        }
	});
        }
}

// For Add Make
function showHideEdit(id){
    var make =  document.getElementById(id).value;
    if(make == 0){
        alert("Please select "+id);
        return false;
    }
    var index = document.getElementById(id).selectedIndex;
    document.getElementById(id+'-id').value = make;
    document.getElementById(id+'-text').value = document.getElementById(id).options[index].text;
    var divId = 'div'+id;
    var make = document.getElementById(divId);
    make.style.display = "block";
}
function addMake(name){
    var addValue = "";
    var parm = "";
    if(name == "model"){
        addValue = document.getElementById("make").value;
        parm = "&make="+addValue+"&model=0&engine=0&year=0";
        if(addValue == "" || addValue == 0){
            alert("Please Select Make");
            return false;
        }
    }
    if(name == "engineno"){
        addValue = document.getElementById("model").value;
        parm = "&make="+document.getElementById("make").value+"&model="+addValue+"&engine=0&year=0";
        if(addValue == "" || addValue == 0){
            alert("Please Select Model");
            return false;
        }
    }
    if(name == "year"){
        addValue = document.getElementById("engineno").value;
        parm = "&make="+document.getElementById("make").value+"&model="+document.getElementById("model").value+"&engine="+addValue+"&year=0";
        if(addValue == "" || addValue == 0){
            alert("Please Select Engine");
            return false;
        }
    }
    var fieldText = name+'-text';
    var fieldId = name+'-id';
    var fieldTextVal = document.getElementById(fieldText).value;
    var fieldIdVal = document.getElementById(fieldId).value;
    if(fieldTextVal == ""){
        alert("Please enter "+name);
        return false;
    }
    //alert(fieldTextVal);
    //alert(fieldIdVal);
    
       var reloadurl = '<?php echo Mage::helper('adminhtml')->getUrl('filter/adminhtml_filter/addoption') ?>';
       var param=  'fieldTextVal='+fieldTextVal+'&fieldIdVal='+fieldIdVal+'&attName='+name+'&attValue='+addValue+parm;
	new Ajax.Request(reloadurl, {
	method: 'post',
	parameters: param,
	onComplete: function(transport) {
                response = transport.responseText;
                if(response == 1){
                    alert("Already Exists.")
                } else {
                    $(name).innerHTML = "";
                    var obj = JSON.parse(response);
                    var combobox = "";
                    var ob;
                    if(name=='engineno'){
                        combobox = '<option value="0">Select Engine</option>';
                    } else {
                        combobox = '<option value="0">Select '+ucfirst(name)+'</option>';
                    }
                    
                    
                    
                    for(ob in obj){
                            combobox =  combobox +'<option value= "'+ob+'">';
                            combobox =  combobox + obj[ob];
                            combobox =  combobox +'</option>';
                    }
                    $(name).innerHTML = combobox;
                }
            }
        });
    document.getElementById(fieldText).value = "";
    document.getElementById(fieldId).value = 0; 
    document.getElementById("div"+name).style.display = "none";
}
// End Add Make
// For Delete Attribute
function attDelete(name){
    var nameVal =  document.getElementById(name).value;
    if(nameVal == 0){
         if(name=='engineno'){
                        name='Engine';
                    }
        alert("Please select "+ucfirst(name));
         if(name=='Engine'){
                        name='engineno';
                    }
        return false;
    }
    var r=confirm("Are you sure you want to delete(s)?");
    if (r==true)
    {
        var parm = "";
        if(name == "model"){
            parm = "&make="+document.getElementById("make").value+"&model=0&engine=0&year=0";
        }
        if(name == "engineno"){
            parm = "&make="+document.getElementById("make").value+"&model="+document.getElementById("model").value+"&engine=0&year=0";
        }
        if(name == "year"){
            parm = "&make="+document.getElementById("make").value+"&model="+document.getElementById("model").value+"&engine="+document.getElementById("engineno").value+"&year=0";
        }
        //alert(nameVal);
        var reloadurl = '<?php echo Mage::helper('adminhtml')->getUrl('filter/adminhtml_filter/deleteoption') ?>';
            var param=  'optionId='+nameVal+'&name='+name+parm;
            new Ajax.Request(reloadurl, {
            method: 'post',
            parameters: param,
            onComplete: function(transport) {
                    response = transport.responseText;
                    //  alert(response);
                    if(response == 1){
                        alert("Already Exists.")
                    } else {
                        $(name).innerHTML = "";
                        var obj = JSON.parse(response);
                        var combobox = "";
                        var ob;
                        //combobox = '<option value="0">Select '+name+'</option>'
                        for(ob in obj){
                                combobox =  combobox +'<option value= "'+ob+'">';
                                combobox =  combobox + obj[ob];
                                combobox =  combobox +'</option>';
                        }
                        $(name).innerHTML = combobox;
                        if(name == "make"){
                            getModelList(document.getElementById("make").value,"");
                            getEngnoList(document.getElementById("model").value,"");
                            getYearList(document.getElementById("engineno").value,"");
                        }
                        if(name == "model"){
                            getEngnoList(document.getElementById("model").value,"");
                            getYearList(document.getElementById("engineno").value,"");
                        }
                        if(name == "engineno"){
                            getYearList(document.getElementById("engineno").value,"");
                        }

                    }
                }
            });
      }
    /*document.getElementById(fieldText).value = "";
    document.getElementById(fieldId).value = 0; 
    document.getElementById("div"+name).style.display = "none";*/
    
}
// End Delete Attribute

function ucfirst(string){
	    return string.charAt(0).toUpperCase() + string.slice(1);
	}


</script>
