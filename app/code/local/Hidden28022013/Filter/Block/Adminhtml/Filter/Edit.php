<?php
	
class Hidden_Filter_Block_Adminhtml_Filter_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
		public function __construct()
		{

				parent::__construct();
				$this->_objectId = "filter_id";
				$this->_blockGroup = "filter";
				$this->_controller = "adminhtml_filter";
				$this->_updateButton("save", "label", Mage::helper("filter")->__("Save Vehicle Selector"));
				$this->_updateButton("delete", "label", Mage::helper("filter")->__("Delete Vehicle Selector"));

				$this->_addButton("saveandcontinue", array(
					"label"     => Mage::helper("filter")->__("Save And Continue Edit"),
					"onclick"   => "saveAndContinueEdit()",
					"class"     => "save",
				), 000);



				$this->_formScripts[] = "
							var editForm = new varienForm('edit_form');
							Validation.addAllThese(
[
['validate-make', 'Please Select Make', function(v) {
if(document.getElementById('make').value == '0' || document.getElementById('make').value == '')
{
    return false;
} else {
    return true;
}
}],
['validate-model', 'Please Select Model', function(v) {
if(document.getElementById('model').value == '0' || document.getElementById('model').value == '')
{
return false;
} else {
    return true;
}
}],
['validate-year', 'Please Select Year', function(v) {
if(document.getElementById('year').value == '0' || document.getElementById('year').value == '')
{
return false;
} else {
    return true;
}
}]  ,
['validate-engineno', 'Please Select Engine', function(v) {
if(document.getElementById('engineno').value == '0' || document.getElementById('engineno').value == '')
{
return false;
} else {
    return true;
}
}]    
])
							
							
							
							function saveAndContinueEdit(){
								editForm.submit($('edit_form').action+'back/edit/');
							}
                                                        // For Get Edit Value
                                                       
                                                        
                                                        document.observe('dom:loaded', function() { 
                                                        var make = document.getElementById('make').value;
                                                        var model = document.getElementById('model').value;
                                                        var engineno = document.getElementById('engineno').value;
                                                        var year = document.getElementById('year').value;
                                                        //alert(engineno)
                                                            //alert(make);
                                                                if(make != ''){
                                                                getModelList(make,model);
                                                                }
                                                                //alert(model);
                                                             /*   if(model != ''){
                                                                    getEngnoList(model,engineno);
                                                                }*/


                                                                //alert(year);
                                                                /*if(engineno != ''){
                                                                    getYearList(engineno,year);
                                                                }*/
                                                        });
						";
		}

		public function getHeaderText()
		{
				if( Mage::registry("filter_data") && Mage::registry("filter_data")->getId() ){

				    return Mage::helper("filter")->__("Edit/Add Vehicle selector (ID:'%s')", $this->htmlEscape(Mage::registry("filter_data")->getId()));

				} 
				else{

				     return Mage::helper("filter")->__("Vehicle Selector");

				}
		}
}

