<?php

class Hidden_Filter_Block_Adminhtml_Filter_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

		public function __construct()
		{
				parent::__construct();
				$this->setId("filterGrid");
				$this->setDefaultSort("filter_id");
				$this->setDefaultDir("ASC");
				$this->setSaveParametersInSession(true);
		}

		protected function _prepareCollection()
		{
				$collection = Mage::getModel("filter/filter")->getCollection()->addFieldToFilter('sku',array('notnull' => true));
                                
				$this->setCollection($collection);
				return parent::_prepareCollection();
		}
		protected function _prepareColumns()
		{
				$this->addColumn("filter_id", array(
				"header" => Mage::helper("filter")->__("ID"),
				"align" =>"right",
				"width" => "50px",
			    "type" => "number",
				"index" => "filter_id",
				));
                
						$this->addColumn('sku', array(
						'header' => Mage::helper('filter')->__('Product Name'),
						'index' => 'sku',
						'type' => 'options',
						'options'=>Hidden_Filter_Block_Adminhtml_Filter_Grid::getOptionArray0(),				
						));
						
						$this->addColumn('make', array(
						'header' => Mage::helper('filter')->__('Make'),
						'index' => 'make',
						'type' => 'options',
						'options'=>Hidden_Filter_Block_Adminhtml_Filter_Grid::getOptionArray1('make'),				
						));
						
						$this->addColumn('model', array(
						'header' => Mage::helper('filter')->__('Model'),
						'index' => 'model',
						'type' => 'options',
						'options'=>Hidden_Filter_Block_Adminhtml_Filter_Grid::getOptionArray1('model'),				
						));
						
						$this->addColumn('year', array(
						'header' => Mage::helper('filter')->__('Year'),
						'index' => 'year',
						'type' => 'options',
						'options'=>Hidden_Filter_Block_Adminhtml_Filter_Grid::getOptionArray1('year'),				
						));
						
						$this->addColumn('engineno', array(
						'header' => Mage::helper('filter')->__('Engine No'),
						'index' => 'engineno',
						'type' => 'options',
						'options'=>Hidden_Filter_Block_Adminhtml_Filter_Grid::getOptionArray1('engineno'),				
						));
                                                
                                                $this->addColumn('action',
                                                    array(
                                                        'header'    => Mage::helper('filter')->__('Action'),
                                                        'width'     => '50px',
                                                        'type'      => 'action',
                                                        'getter'     => 'getId',
                                                        'actions'   => array(
                                                            array(
                                                                'caption' => Mage::helper('catalog')->__('Edit'),
                                                                'url'     => array(
                                                                    'base'=>'*/*/edit',
                                                                    'params'=>array('store'=>$this->getRequest()->getParam('store'))
                                                                ),
                                                                'field'   => 'id'
                                                            )
                                                        ),
                                                        'filter'    => false,
                                                        'sortable'  => false,
                                                        'index'     => 'stores',
                                                ));
						

				return parent::_prepareColumns();
		}

		public function getRowUrl($row)
		{
			   return $this->getUrl("*/*/edit", array("id" => $row->getId()));
		}


		
		protected function _prepareMassaction()
		{
			$this->setMassactionIdField('filter_id');
			$this->getMassactionBlock()->setFormFieldName('filter_ids');
			$this->getMassactionBlock()->setUseSelectAll(true);
			$this->getMassactionBlock()->addItem('remove_filter', array(
					 'label'=> Mage::helper('filter')->__('Remove Filter'),
					 'url'  => $this->getUrl('*/adminhtml_filter/massRemove'),
					 'confirm' => Mage::helper('filter')->__('Are you sure?')
				));
			return $this;
		}
			
		static public function getOptionArray0()
		{
                    $data_array=array();
                    $model      = Mage::getModel('catalog/product');
                    $collection = $model->getCollection();
                    foreach ($collection as $product) {
                        $id   = $product->getId();
                        $model->load($id);
                        $data_array[$product->getId()] = $model->getName();
                    }
                    
                    return($data_array);
		}
                static public function getValueArrayEdit()
		{
                    $data_array=array();
                    $model      = Mage::getModel('catalog/product');
                    $collection = $model->getCollection();
                    foreach ($collection as $product) {
                        $id   = $product->getId();
                        
                        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
                        $select ="select sku from filter where sku='".$id."'";
                        $rowArray =$connection->fetchRow($select);
                                $model->load($id);
                                $data_array[$product->getId()] = $model->getName();
                        }
                    
                    return($data_array);
		}
		
		static public function getValueArray0()
		{
                    $data_array=array();
                    $model      = Mage::getModel('catalog/product');
                    $collection = $model->getCollection();
                    foreach ($collection as $product) {
                        $id   = $product->getId();
                        
                        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
                        $select ="select sku from filter where sku='".$id."'";
                        $rowArray =$connection->fetchRow($select);
                         if($rowArray['sku']!= $id ){
                                $model->load($id);
                                $data_array[$product->getId()] = $model->getName();
                        }
                            
                    }
                    
                    return($data_array);
		}
		
		static public function getOptionArray2()
		{
                    $data_array=array();
                    $model = Mage::getModel("filter/filter");
                    $collection = $model->getCollection();
                    $data_array[-1] = 'Select Model';
                    foreach ($collection as $product) {
                        $id   = $product->getId();
                        $model->load($id);
                        $modelValue=$model->getModel();
                        $data_array[$modelValue] = $modelValue;
                    }
                    return($data_array);
		}
		static public function getValueArray2()
		{
                    $data_array=array();
                    $model = Mage::getModel("filter/filter");
                    $collection = $model->getCollection();
                    $data_array[-1] = 'Select Model';
                    foreach ($collection as $product) {
                        $id   = $product->getId();
                        $model->load($id);
                        $modelValue=$model->getModel();
                        $data_array[$modelValue] = $modelValue;
                    }
                    return($data_array);
		}
		
		static public function getOptionArray1($attr)
		{
                    
                    $data_array=array();
                    $attribute = Mage::getSingleton('eav/config')->getAttribute('catalog_product',$attr);
                    if ($attribute->usesSource()) {
                        $options = $attribute->getSource()->getAllOptions(false);
                        $cnt=0;
                        for($j=0;$j<count($options);$j++){
                                $makeoption=$options[$j]['value'];
                                $data_array[$makeoption]=$options[$j]['label'];
                        }
                    }
                   /* $model = Mage::getModel("filter/filter");
                    $collection = $model->getCollection();
                    $data_array[-1] = 'Select Make';
                    foreach ($collection as $product) {
                        $id   = $product->getId();
                        $model->load($id);
                        $makeValue=$model->getMake();
                        $data_array[$makeValue] = $makeValue;
                    }*/
                    return($data_array);
		}
		static public function getValueArray1($attr,$id)
		{
                    
                    $data_array=array();
                    $attribute = Mage::getSingleton('eav/config')->getAttribute('catalog_product',$attr);
                    if ($attribute->usesSource()) {
                        $options = $attribute->getSource()->getAllOptions(false);
                        $cnt=0;
                        $data_array[0]='Select '.$attr;
                        if($attr=='make' || $id!=""){
                            for($j=0;$j<count($options);$j++){
                                    $makeoption=$options[$j]['value'];
                                    $data_array[$makeoption]=$options[$j]['label'];
                            }
                        }
                    }
                   /* $model = Mage::getModel("filter/filter");
                    $collection = $model->getCollection();
                    $data_array[-1] = 'Select Make';
                    foreach ($collection as $product) {
                        $id   = $product->getId();
                        $model->load($id);
                        $makeValue=$model->getMake();
                        $data_array[$makeValue] = $makeValue;
                    }*/
                    return($data_array);
                    /*$data_array=array();
                    $model = Mage::getModel("filter/filter");
                    $data_array[-1] = 'Select Make';
                    $collection = $model->getCollection();
                    foreach ($collection as $product) {
                        $id   = $product->getId();
                        $model->load($id);
                        $makeValue=$model->getMake();
                        $data_array[$makeValue] = $makeValue;
                    }
                    return($data_array);*/
		}
		
		static public function getOptionArray4()
		{
                    $data_array=array();
                    $model = Mage::getModel("filter/filter");
                    $collection = $model->getCollection();
                    $data_array[-1] = 'Select Year';
                    foreach ($collection as $product) {
                        $id   = $product->getId();
                        $model->load($id);
                        $yearValue=$model->getYear();
                        $data_array[$yearValue] = $yearValue;
                    }
                    return($data_array);
		}
		static public function getValueArray4()
		{
                    $data_array=array();
                    $model = Mage::getModel("filter/filter");
                    $collection = $model->getCollection();
                    $data_array[-1] = 'Select Year';
                    foreach ($collection as $product) {
                        $id   = $product->getId();
                        $model->load($id);
                        $yearValue=$model->getYear();
                        $data_array[$yearValue] = $yearValue;
                    }
                    return($data_array);
		}
		
		static public function getOptionArray5()
		{
                    $data_array=array();
                    $model = Mage::getModel("filter/filter");
                    $collection = $model->getCollection();
                    $data_array[-1] = 'Select Engine No';
                    foreach ($collection as $product) {
                        $id   = $product->getId();
                        $model->load($id);
                        $enginenoValue=$model->getEngineno();
                        $data_array[$enginenoValue] = $enginenoValue;
                    }
                    return($data_array);
		}
		static public function getValueArray5()
		{
                    $data_array=array();
                    $model = Mage::getModel("filter/filter");
                    $collection = $model->getCollection();
                    $data_array[-1] = 'Select Engine No';
                    foreach ($collection as $product) {
                        $id   = $product->getId();
                        $model->load($id);
                        $enginenoValue=$model->getEngineno();
                        $data_array[$enginenoValue] = $enginenoValue;
                    }
                    return($data_array);
		}
		

}