<?php
class Hidden_Filter_Block_Adminhtml_Filter_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
		protected function _prepareForm()
		{

				$form = new Varien_Data_Form();
                                $this->setForm($form);
				$fieldset = $form->addFieldset("filter_form", array("legend"=>Mage::helper("filter")->__("Vehicle selector information")));
                                //ECHO "<PRE>";print_r(Mage::registry("filter_data")->getData());ECHO "</PRE>";exit;
                                
                                $value=Hidden_Filter_Block_Adminhtml_Filter_Grid::getValueArrayEdit();
                                $makelink='<button  class="scalable " type="button" title="Add Option"  onclick ="showhide(\'divmake\')"/><span><span><span>Add</span></span></span></button>&nbsp;<button  class="scalable " type="button" title="Add Option"  onclick ="showHideEdit(\'make\')"/><span><span><span>Edit</span></span></span></button>&nbsp;<button  class="scalable " type="button" title="Add Option"  onclick ="attDelete(\'make\')"/><span><span><span>Delete</span></span></span></button><div id="divmake" style="display:none;float: right; padding: 0 0 0 10px;"><input type="text" class=" input-text" value="" name="make-text" id="make-text"><input type="hidden" value="0" name="make-id" id="make-id">&nbsp;<button  class="scalable " type="button" title="Save" onclick="return addMake(\'make\');"/><span><span><span>Save</span></span></span></button></div>';
                                    $modellink='<button  class="scalable " type="button" title="Add Option"  onclick ="showhide(\'divmodel\')"><span><span><span>Add</span></span></span></button>&nbsp;<button  class="scalable " type="button" title="Add Option"  onclick ="showHideEdit(\'model\')"/><span><span><span>Edit</span></span></span></button>&nbsp;<button  class="scalable " type="button" title="Add Option"  onclick ="attDelete(\'model\')"/><span><span><span>Delete</span></span></span></button><div id="divmodel" style="display:none;float: right; padding: 0 0 0 10px;"><input type="text" class=" input-text" value="" name="model-text" id="model-text"><input type="hidden" value="0" name="model-id" id="model-id">&nbsp;<button  class="scalable " type="button" title="Save" onclick="return addMake(\'model\');"/><span><span><span>Save</span></span></span></button></div>';
                                    $yearlink='<button  class="scalable " type="button" title="Add Option"  onclick ="showhide(\'divyear\')"><span><span><span>Add</span></span></span></button>&nbsp;<button  class="scalable " type="button" title="Add Option"  onclick ="showHideEdit(\'year\')"/><span><span><span>Edit</span></span></span></button>&nbsp;<button  class="scalable " type="button" title="Add Option"  onclick ="attDelete(\'year\')"/><span><span><span>Delete</span></span></span></button><div id="divyear" style="display:none;float: right; padding: 0 0 0 10px;"><input type="text" class=" input-text" value="" name="year-text" id="year-text"><input type="hidden" value="0" name="year-id" id="year-id">&nbsp;<button  class="scalable " type="button" title="Save" onclick="return addMake(\'year\');"/><span><span><span>Save</span></span></span></button></div>';
                                    $engnolink='<button  class="scalable " type="button" title="Add Option"  onclick ="showhide(\'divengineno\')"><span><span><span>Add</span></span></span></button>&nbsp;<button  class="scalable " type="button" title="Add Option"  onclick ="showHideEdit(\'engineno\')"/><span><span><span>Edit</span></span></span></button>&nbsp;<button  class="scalable " type="button" title="Add Option"  onclick ="attDelete(\'engineno\')"/><span><span><span>Delete</span></span></span></button><div id="divengineno" style="display:none;float: right; padding: 0 0 0 10px;"><input type="text" class=" input-text" value="" name="engineno-text" id="engineno-text"><input type="hidden" value="0" name="engineno-id" id="engineno-id">&nbsp;<button  class="scalable " type="button" title="Save" onclick="return addMake(\'engineno\');"/><span><span><span>Save</span></span></span></button></div>';
                                if($this->getRequest()->getParam("id")){
                                $fieldset->addField('sku', 'hidden', array(
                                    'label'     => Mage::helper('filter')->__('Product Name'),
                                    'values'   => '',
                                    'name' => 'sku',					

                                    ));	
                                }
                                
                                if($this->getRequest()->getParam("id")){
                                    $getDataArr=Mage::registry("filter_data")->getData();
                                    $sku=$getDataArr['sku'];
                                    $make=$getDataArr['make'];
                                    $fieldset->addField('sku1', 'link', array(
                                    'label'     => Mage::helper('filter')->__('Product Name'),
                                    'values'   => '',
                                    'name' => 'sku1',					
                                    'after_element_html' => $value[$sku]
                                    ));	


                                } elseif($this->getRequest()->getParam("pid")){
                                     $fieldset->addField('sku', 'hidden', array(
                                    'label'     => Mage::helper('filter')->__('Product Name'),
                                    'values'   => '',
                                    'name' => 'sku',					

                                    ));	
                                    $id=$this->getRequest()->getParam("pid");
                                    $model      = Mage::getModel('catalog/product')->load($id);
                                    
                                    $getDataArr=$model->getData();
                                    //echo "<pre>";print_r($getDataArr);exit;
                                    //$getDataArr=Mage::registry("filter_data")->getData();
                                     $sku=$getDataArr['entity_id'];
                                     
                                     
                                     $fieldset->addField('sku1', 'link', array(
                                    'label'     => Mage::helper('filter')->__('Product Name'),
                                    'values'   => '',
                                    'name' => 'sku1',					
                                    'after_element_html' => $value[$sku]."<script>document.getElementById('sku').value ='".$sku."'; </script>"
                                    ));	
                                } else {	
                                    $fieldset->addField('sku', 'select', array(
                                    'label'     => Mage::helper('filter')->__('Product Name'),
                                    'values'   => Hidden_Filter_Block_Adminhtml_Filter_Grid::getValueArray0(),
                                    'name' => 'sku',					
                                    "class" => "required-entry",
                                    "required" => true,

                                    ));	
                                    

                                }

                                $fieldset->addField('make', 'select', array(
                                'label'     => Mage::helper('filter')->__('Make'),
                                'values'   => Hidden_Filter_Block_Adminhtml_Filter_Grid::getValueArray1('make',$this->getRequest()->getParam("id")),
                                'name' => 'make',
                                'class' => 'validate-make',
                                'required' => true,
                                'after_element_html' => $makelink,
                                'onchange'=>"getModelList(this.value)"
                                ));
                                    $fieldset->addField('model', 'select', array(
                                'label'     => Mage::helper('filter')->__('Model'),
                                'values'   => Hidden_Filter_Block_Adminhtml_Filter_Grid::getValueArray1('model',$this->getRequest()->getParam("id")),
                                'name' => 'model',
                                'after_element_html' => $modellink,
                                'onchange'=>"getEngnoList(this.value)",
                                'class' => 'validate-model',
                                'required' => true,
                                ));	
                                $fieldset->addField('engineno', 'select', array(
                                'label'     => Mage::helper('filter')->__('Engine'),
                                'values'   => Hidden_Filter_Block_Adminhtml_Filter_Grid::getValueArray1('engineno',$this->getRequest()->getParam("id")),
                                'name' => 'engineno',
                                'after_element_html' => $engnolink,
                                'onchange'=>"getYearList(this.value)",
                                'class' => 'validate-engineno',
                                'required' => true,
                                ));
                                    $fieldset->addField('year', 'select', array(
                                'label'     => Mage::helper('filter')->__('Year'),
                                'values'   => Hidden_Filter_Block_Adminhtml_Filter_Grid::getValueArray1('year',$this->getRequest()->getParam("id")),
                                'name' => 'year',
                                'after_element_html' => $yearlink,
                               
                                'class' => 'validate-year',
                                'required' => true,
                                ));	

                               
                                                		
                                                 
                                                 

				if (Mage::getSingleton("adminhtml/session")->getFilterData())
				{
					$form->setValues(Mage::getSingleton("adminhtml/session")->getFilterData());
					Mage::getSingleton("adminhtml/session")->setFilterData(null);
				} 
				elseif(Mage::registry("filter_data")) {
				    $form->setValues(Mage::registry("filter_data")->getData());
				}
				return parent::_prepareForm();
		}
}
?>
