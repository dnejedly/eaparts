<?php

class Hidden_Filter_Adminhtml_FilterController extends Mage_Adminhtml_Controller_Action
{
		protected function _initAction()
		{
				$this->loadLayout()->_setActiveMenu("catalog/filter")->_addBreadcrumb(Mage::helper("adminhtml")->__("Filter  Manager"),Mage::helper("adminhtml")->__("Filter Manager"));
				return $this;
		}
		public function indexAction() 
		{
			    $this->_title($this->__("Filter"));
			    $this->_title($this->__("Manager Vehicle Selector"));

				$this->_initAction();
				$this->renderLayout();
		}
		public function editAction()
		{			    
                $this->_title($this->__("Filter"));
				$this->_title($this->__("Filter"));
			    $this->_title($this->__("Edit Vehicle"));
				
				$id = $this->getRequest()->getParam("id");
				$model = Mage::getModel("filter/filter")->load($id);
				if ($model->getId()) {
					Mage::register("filter_data", $model);
					$this->loadLayout();
					
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Filter Manager"), Mage::helper("adminhtml")->__("Filter Manager"));
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Filter Description"), Mage::helper("adminhtml")->__("Filter Description"));
					$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
                                        $this->loadLayout()->_setActiveMenu("catalog/filter");
					$this->_addContent($this->getLayout()->createBlock("filter/adminhtml_filter_edit"))->_addLeft($this->getLayout()->createBlock("filter/adminhtml_filter_edit_tabs"));
					$this->renderLayout();
				} 
				else {
					Mage::getSingleton("adminhtml/session")->addError(Mage::helper("filter")->__("Item does not exist."));
					$this->_redirect("*/*/");
				}
		}
                public function editfilterAction()
		{	
                    //print_r($this->getRequest()->getParam("id"));exit;
                    
                            $this->_title($this->__("Filter"));
				$this->_title($this->__("Filter"));
			    $this->_title($this->__("Edit Vehicle"));
				
				$id = $this->getRequest()->getParam("pid");
                                $model = Mage::getModel("filter/filter")->getCollection();
                                $model->addFilter('sku',$id );
                                $filterArr=$model->getData();
                                //echo "<pre>";print_r($filterArr[0]['filter_id']);
				//$model = Mage::getModel("filter/filter")->load($id);
                                
                                
                               
				if ($filterArr[0]['filter_id']) {
                                        $model = Mage::getModel("filter/filter")->load($filterArr[0]['filter_id']);
					Mage::register("filter_data", $model);
					$this->loadLayout();
					
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Filter Manager"), Mage::helper("adminhtml")->__("Filter Manager"));
					$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Filter Description"), Mage::helper("adminhtml")->__("Filter Description"));
					$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);
                                        $this->loadLayout()->_setActiveMenu("catalog/filter");
					$this->_addContent($this->getLayout()->createBlock("filter/adminhtml_filter_edit"))->_addLeft($this->getLayout()->createBlock("filter/adminhtml_filter_edit_tabs"));
					$this->renderLayout();
				} 
				else {
					$this->newAction();

                                    }
		}

		public function newAction()
		{

		$this->_title($this->__("Filter"));
		$this->_title($this->__("Filter"));
		$this->_title($this->__("New Item"));

        $id   = $this->getRequest()->getParam("id");
		$model  = Mage::getModel("filter/filter")->load($id);

		$data = Mage::getSingleton("adminhtml/session")->getFormData(true);
		if (!empty($data)) {
			$model->setData($data);
		}

		Mage::register("filter_data", $model);

		$this->loadLayout();
		$this->_setActiveMenu("catalog/filter");

		$this->getLayout()->getBlock("head")->setCanLoadExtJs(true);

		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Filter Manager"), Mage::helper("adminhtml")->__("Filter Manager"));
		$this->_addBreadcrumb(Mage::helper("adminhtml")->__("Filter Description"), Mage::helper("adminhtml")->__("Filter Description"));


		$this->_addContent($this->getLayout()->createBlock("filter/adminhtml_filter_edit"))->_addLeft($this->getLayout()->createBlock("filter/adminhtml_filter_edit_tabs"));

		$this->renderLayout();

		}
                function getLastInsertId($tableName, $primaryKey)
{
        //SELECT MAX(id) FROM table
        $db = Mage::getModel('core/resource')->getConnection('core_read');
        $result = $db->raw_fetchRow("SELECT MAX(`{$primaryKey}`) as LastID FROM `{$tableName}`");
        return $result['LastID'];
}
		public function saveAction()
		{

			$post_data=$this->getRequest()->getPost();


				if ($post_data) {
                                    

					try {
                                            
                                                    $product = Mage::getModel('catalog/product');
                                                    $id = $post_data['sku'];
                                                    $product->load($id);
                                                    if($post_data['make-text']==""){
                                                        if($post_data['make']=="0"){
                                                            Mage::getSingleton("adminhtml/session")->addError('Please select Make.');
                                                            $this->_redirect("*/*/edit");
                                                            return;
                                                        }
                                                        $post_data['make']=$post_data['make'];
                                                    }else{
                                                        $post_data['make']=$post_data['make-text'];
                                                    }

                                                    if($post_data['model-text']==""){
                                                        if($post_data['model']=="0"){
                                                            Mage::getSingleton("adminhtml/session")->addError('Please select Model.');
                                                            $this->_redirect("*/*/edit");
                                                            return;
                                                        }
                                                        $post_data['model']=$post_data['model'];
                                                    }else{
                                                        $post_data['model']=$post_data['model-text'];
                                                    }
                                                    if($post_data['year-text']==""){
                                                        if($post_data['year']=="0"){
                                                            Mage::getSingleton("adminhtml/session")->addError('Please select Year.');
                                                            $this->_redirect("*/*/edit");
                                                            return;
                                                        }
                                                        $post_data['year']=$post_data['year'];
                                                    }else{
                                                        $post_data['year']=$post_data['year-text'];
                                                    }
                                                    if($post_data['engineno-text']==""){
                                                        if($post_data['engineno']=="0"){
                                                            Mage::getSingleton("adminhtml/session")->addError('Please select Engine');
                                                            $this->_redirect("*/*/edit");
                                                            return;
                                                        }
                                                        $post_data['engineno']=$post_data['engineno'];
                                                    }else{
                                                        $post_data['engineno']=$post_data['engineno-text'];
                                                    }

                                                    /*$connection = Mage::getSingleton('core/resource')->getConnection('core_read');

                                                    $select ="select COUNT(*) AS total from filter where sku='".$post_data['sku']."' and make='".$post_data['make']."' and model='".$post_data['model']."' and year='".$post_data['year']."' and engineno='".$post_data['engineno']."'";

                                                    $rowArray =$connection->fetchRow($select);*/

                                                    //print_r($post_data);exit;

                                                    $lastInsertId="";
                                                    $argArr = array('make','model','year','engineno');
                                                    for($i=0;$i<count($argArr);$i++){

                                                        //$arg_attribute = 'make';
                                                        $arg_attribute=$argArr[$i];
                                                        $arg_value = $post_data[$arg_attribute];
                                                        $attr_model = Mage::getModel('catalog/resource_eav_attribute');
                                                        $attr = $attr_model->loadByCode('catalog_product', $arg_attribute);
                                                        $attr_id = $attr->getAttributeId();
                                                       $attribute = Mage::getSingleton('eav/config')->getAttribute('catalog_product', $argArr[$i]);
                                                        if ($attribute->usesSource()) {
                                                            $options = $attribute->getSource()->getAllOptions(false);
                                                            $cnt=0;
                                                            for($j=0;$j<count($options);$j++){
                                                                if($options[$j]['value']==$arg_value){
                                                                    $cnt++;
                                                                    $lastInsertId=$options[$j]['value'];
                                                                }
                                                                    
                                                            }
                                                        }
                                                        $setup = new Mage_Eav_Model_Entity_Setup('core_setup');
                                                       
                                                        if($cnt==0){
                                                        $option = array();
                                                        $option['attribute_id'] = $attr_id;
                                                        $option['value'][$arg_attribute.'_option'][0] = $arg_value;
                                                        $setup->addAttributeOption($option);
                                                        }
                                                       
                                                        $optionTable = $setup->getTable('eav/attribute_option');
                                                        if($lastInsertId == ""){
                                                            $lastInsertId = $this->getLastInsertId($optionTable, 'option_id');
                                                        
                                                        }
                                                     //   echo "<pre>";print_r($lastInsertId);
                                                        $product->addAttributeUpdate($argArr[$i],$lastInsertId,0);
                                                        $product->save();
                                                        $lastInsertId='';

                                                    }
                                                    //print_r($post_data);exit;

                                                    $model = Mage::getModel("filter/filter")
                                                    ->addData($post_data)
                                                    ->setId($this->getRequest()->getParam("id"))
                                                    ->save();

                                                    Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Filter was successfully saved"));
                                                    Mage::getSingleton("adminhtml/session")->setFilterData(false);

                                                    if ($this->getRequest()->getParam("back")) {
                                                            $this->_redirect("*/*/edit", array("id" => $model->getId()));
                                                            return;
                                                    }
                                                    $this->_redirect("*/*/");
                                                    return;
                                               
					} 
					catch (Exception $e) { 
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						Mage::getSingleton("adminhtml/session")->setFilterData($this->getRequest()->getPost());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					return;
					}

				}
				$this->_redirect("*/*/");
		}

                
		public function deleteAction()
		{
				if( $this->getRequest()->getParam("id") > 0 ) {
					try {
						$model = Mage::getModel("filter/filter");
						$model->setId($this->getRequest()->getParam("id"))->delete();
						Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item was successfully deleted"));
						$this->_redirect("*/*/");
					} 
					catch (Exception $e) {
						Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
						$this->_redirect("*/*/edit", array("id" => $this->getRequest()->getParam("id")));
					}
				}
				$this->_redirect("*/*/");
		}

		
		public function massRemoveAction()
		{
			try {
				$ids = $this->getRequest()->getPost('filter_ids', array());
				foreach ($ids as $id) {
                      $model = Mage::getModel("filter/filter");
					  $model->setId($id)->delete();
				}
				Mage::getSingleton("adminhtml/session")->addSuccess(Mage::helper("adminhtml")->__("Item(s) was successfully removed"));
			}
			catch (Exception $e) {
				Mage::getSingleton("adminhtml/session")->addError($e->getMessage());
			}
			$this->_redirect('*/*/');
		}
                public function attributevalueAction($id)
    {
        
        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
        $select ="select value from eav_attribute_option_value where option_id='".$id."'";
        $attrValue =$connection->fetchRow($select);
        return $attrValue;
    }
    public function modelAction()
    {
            $data_array=array();
            $make = $this->getRequest()->getParam('make');
            $model = $this->getRequest()->getParam('model');
            $engineno = $this->getRequest()->getParam('engineno');
            $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
            $select ="select * from filter where make='".$make."'";

            if($model!='0'){
                $data_array[''] = 'Select Engine';
                $select .="AND model='".$model."'";
            }
            if($engineno  != '0'){
                $data_array[''] = 'Select Engine';
                $select .="AND engineno='".$engineno."'";
            }
            $rowArray =$connection->fetchAll($select);

            $data_array[''] = "Select Model";

            if(count($rowArray)>0)
            {
                foreach ($rowArray as $product) {
                    $model=$product['model'];
                        if($model!="" && $model!="0"){
                        $modelValue=  $this->attributevalueAction($model);
                        $data_array[$model] = $modelValue['value'];
                        }
                }

            }
            echo json_encode($data_array);
            exit;

    }

    public function enginenoAction()
    {
            $data_array=array();
            $make = $this->getRequest()->getParam('make');
            $model = $this->getRequest()->getParam('model');
            $engineno = $this->getRequest()->getParam('engineno');
            $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
            $select ="select * from filter where make='".$make."'";

            if($model!='0'){
                $data_array[''] = 'Select Year';
                $select .="AND model='".$model."'";
            }
            if($engineno  != '0'){
                $data_array[''] = 'Select Engine';
                $select .="AND engineno='".$engineno."'";
            }
            $rowArray =$connection->fetchAll($select);
            $data_array[''] = "Select Engine";
            if(count($rowArray)>0)
            {
                foreach ($rowArray as $product) {
                    $engineno=$product['engineno'];
                        if($engineno!="" && $engineno!="0"){
                            $enginenoValue=  $this->attributevalueAction($engineno);
                            $data_array[$engineno] = $enginenoValue['value'];
                        }
                }

            }
            echo json_encode($data_array);
            exit;

    }
    public function yearAction()
    {
            $data_array=array();
            $make = $this->getRequest()->getParam('make');
            $model = $this->getRequest()->getParam('model');
            $engineno = $this->getRequest()->getParam('engineno');
            $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
            $select ="select * from filter where make='".$make."'";

            if($model!='0'){
                $data_array[''] = 'Select Year';
                $select .="AND model='".$model."'";
            }
            if($engineno  != '0'){
                $data_array[''] = 'Select Engine';
                $select .="AND engineno='".$engineno."'";
            }
            $rowArray =$connection->fetchAll($select);

            $data_array[''] = "Select Year";
            if(count($rowArray)>0)
            {
                foreach ($rowArray as $product) {
                    $year=$product['year'];
                    if($year!="" && $year!="0"){
                        $yearValue=  $this->attributevalueAction($year);
                        $data_array[$year] = $yearValue['value'];
                    }
                }

            }
            echo json_encode($data_array);
            exit;

    }
                        
                      //  $as_response = array('id'=>'size','id1'=>'size1');
                        
		
                public function addoptionAction()
		{
                    /*
                      <pre>Array
(
    [fieldTextVal] => test
    [fieldIdVal] => 0
    [attName] => model
    [attValue] => 453
    [form_key] => kHaqsOwlU3aYQp3H
)
                     */
                   
                    $fieldTextVal = trim($this->getRequest()->getParam('fieldTextVal'));
                    $fieldIdVal = trim($this->getRequest()->getParam('fieldIdVal'));
                    $attName = trim($this->getRequest()->getParam('attName'));
                    $attValue = trim($this->getRequest()->getParam('attValue'));
                    $make = trim($this->getRequest()->getParam('make'));
                    $model = trim($this->getRequest()->getParam('model'));
                    $engine = trim($this->getRequest()->getParam('engine'));
                    $year = trim($this->getRequest()->getParam('year'));
                    
                    
                    if($attName == "model"):
                        $filterName = "make";
                    elseif($attName == "engineno"):
                        $filterName = "model";
                    elseif($attName == "year"):
                        $filterName = "engineno";
                    else:
                        $filterName = "";
                    endif;
                    // For Get Filter Array
                    // This Condidtion For Make
                    if($filterName != ""){
                        // Get Attribute option by id.
                        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
                        $select ="select * from filter where ".$filterName."='".$attValue."'";
                        $rowArray =$connection->fetchAll($select);
                        $data_array=array();
                        if($attName=='engineno'){
                            $data_array[''] = "Select Engine";
                        } else {
                            $data_array[''] = "Select ".ucfirst($attName);
                        }
                        
                        $filter = array();
                        if(count($rowArray)>0)
                        {
                            foreach ($rowArray as $product) {
                                $attoptValue=$product[$attName];
                                $enginenoselect ="select value from eav_attribute_option_value where option_id='".$attoptValue."'";
                                $enginenoValue =$connection->fetchAll($enginenoselect);
                                if($attoptValue != 0){
                                    $filter[$attoptValue] = $enginenoValue[0]['value'];
                                }
                            }
                        }
                        if(in_array($fieldTextVal,$filter)){
                            echo "1";
                            exit;
                        } else {
                            
                            $attr_model = Mage::getModel('catalog/resource_eav_attribute');
                            $attr = $attr_model->loadByCode('catalog_product',$this->getRequest()->getParam('attName'));
                            $attr_id = $attr->getAttributeId();
                            $attribute = Mage::getSingleton('eav/config')->getAttribute('catalog_product', $this->getRequest()->getParam('attName'));
                            if ($attribute->usesSource()) {
                                $options = $attribute->getSource()->getAllOptions(false);
                                $cnt=0;
                                for($j=0;$j<count($options);$j++){
                                    $attrId=$options[$j]['value'];
                                    $data_array[$attrId] = $options[$j]['label'];
                                    if($options[$j]['label']==$fieldTextVal){
                                        $cnt++;
                                        $lastInsertId=$options[$j]['value'];
                                    }

                                }
                            }
                            if($fieldIdVal==0){    
                                // Save Attribute option 
                                $setup = new Mage_Eav_Model_Entity_Setup('core_setup');
                                if($cnt==0){
                                    $option = array();
                                    $option['attribute_id'] = $attr_id;
                                    $option['value']['make_option'][0] = $fieldTextVal;
                                //   echo "<pre>";print_r($option);exit;
                                    $setup->addAttributeOption($option);
                                    $optionTable = $setup->getTable('eav/attribute_option');
                                    $lastInsertId = $this->getLastInsertId($optionTable, 'option_id');

                                    $filter[$lastInsertId] = $fieldTextVal;
                                }

                                // For Inserrt Value in Filter table
                                if($attName == "model"):
                                    $val = "values (".$make.",".$lastInsertId.",'0','0')";
                                elseif($attName == "engineno"):
                                    $val = "values (".$make.",".$model.",".$lastInsertId.",'0')";
                                elseif($attName == "year"):
                                    $val = "values (".$make.",".$model.",".$engine.",".$lastInsertId.")";
                                endif;
                                $write = Mage::getSingleton('core/resource')->getConnection('core_write');
                                $sql  = "INSERT INTO filter (make,model,engineno,year) $val";
                                $write->query($sql); 
                                // 
                            }else{
                                if($cnt==0){
                                    $write = Mage::getSingleton('core/resource')->getConnection('core_write');
                                    $sql  = "UPDATE eav_attribute_option_value SET value = '".$fieldTextVal."' WHERE option_id=".$fieldIdVal;
                                    $write->query($sql); 
                                    $attribute = Mage::getModel('eav/config')->getAttribute('catalog_product', $this->getRequest()->getParam('attName'));
                                    $options= $attribute->getSource()->getAllOptions(true);

                                    for($j=0;$j<count($options);$j++){
                                        if($options[$j]['value']!=""){
                                            $attrId=$options[$j]['value'];
                                            $filter[$attrId] = $options[$j]['label'];
                                        }

                                    }
                                }
                            }
                            echo json_encode($filter);
                            exit;
                        }
                        echo json_encode($data_array);exit;
                            
                        
                    }else{
                        $data_array=array();


                        $attr_model = Mage::getModel('catalog/resource_eav_attribute');
                        $attr = $attr_model->loadByCode('catalog_product',$this->getRequest()->getParam('attName'));
                        $attr_id = $attr->getAttributeId();
                        $attribute = Mage::getSingleton('eav/config')->getAttribute('catalog_product', $this->getRequest()->getParam('attName'));
                        if ($attribute->usesSource()) {
                            $options = $attribute->getSource()->getAllOptions(false);
                            $cnt=0;
                            for($j=0;$j<count($options);$j++){
                                $attrId=$options[$j]['value'];
                                $data_array[$attrId] = $options[$j]['label'];
                                if($options[$j]['label']==$fieldTextVal){
                                    $cnt++;
                                    $lastInsertId=$options[$j]['value'];
                                }

                            }
                        }
                        if($fieldIdVal==0){    
                            $setup = new Mage_Eav_Model_Entity_Setup('core_setup');

                            if($cnt==0){
                                $option = array();
                                $option['attribute_id'] = $attr_id;
                                $option['value']['make_option'][0] = $fieldTextVal;
                                $setup->addAttributeOption($option);
                                $optionTable = $setup->getTable('eav/attribute_option');
                                $lastInsertId = $this->getLastInsertId($optionTable, 'option_id');
                                $data_array[$lastInsertId] = $fieldTextVal;
                                echo json_encode($data_array);
                                exit;
                            }else{
                                echo "1";
                                exit;
                            }
                        }else{
                            if($cnt==0){
                                $write = Mage::getSingleton('core/resource')->getConnection('core_write');
                                $sql  = "UPDATE eav_attribute_option_value SET value = '".$fieldTextVal."' WHERE option_id=".$fieldIdVal;
                                $write->query($sql); 
                                $attribute = Mage::getModel('eav/config')->getAttribute('catalog_product', $this->getRequest()->getParam('attName'));
                                $options= $attribute->getSource()->getAllOptions(true);

                                for($j=0;$j<count($options);$j++){
                                    if($options[$j]['value']!=""){
                                        $attrId=$options[$j]['value'];
                                        $data_array[$attrId] = $options[$j]['label'];
                                    }

                                }

                                echo json_encode($data_array);
                                exit;
                            } else {
                                echo "1";
                                exit;
                            }
                        }
                 
                    }
                     
                    
                }
                public function deleteoptionAction()
                {
                    
                      $optionId = trim($this->getRequest()->getParam('optionId'));
                      $name = trim($this->getRequest()->getParam('name'));
                      $make = trim($this->getRequest()->getParam('make'));
                      $model = trim($this->getRequest()->getParam('model'));
                      $engine = trim($this->getRequest()->getParam('engine'));
                      $year = trim($this->getRequest()->getParam('year'));
                      // Get Attribute option by id.
                        $connection = Mage::getSingleton('core/resource')->getConnection('core_read');
                        $select ="select * from filter where ".$name."='".$optionId."'";
                        $rowArray =$connection->fetchAll($select);
                        $attrOptionValue=array();
                        $attrOptionValue[]=$optionId;
                        
                           
                        
                        if(count($rowArray)>0)
                        {
                            foreach ($rowArray as $product) {
                                
                                if($name=="make"){
                                    if(!in_array($product['make'],$attrOptionValue)){
                                        $attrOptionValue[]=$product['make'];
                                    }
                                    if(!in_array($product['model'],$attrOptionValue)){
                                        $attrOptionValue[]=$product['model'];
                                    }
                                    if(!in_array($product['year'],$attrOptionValue)){
                                        $attrOptionValue[]=$product['year'];
                                    }
                                    if(!in_array($product['engineno'],$attrOptionValue)){
                                        $attrOptionValue[]=$product['engineno'];
                                    }
                                }
                                if($name=="model"){
                                    if(!in_array($product['model'],$attrOptionValue)){
                                        $attrOptionValue[]=$product['model'];
                                    }
                                    if(!in_array($product['year'],$attrOptionValue)){
                                        $attrOptionValue[]=$product['year'];
                                    }
                                    if(!in_array($product['engineno'],$attrOptionValue)){
                                        $attrOptionValue[]=$product['engineno'];
                                    }
                                }
                                if($name=="engineno"){
                                    if(!in_array($product['engineno'],$attrOptionValue)){
                                        $attrOptionValue[]=$product['engineno'];
                                    }
                                    if(!in_array($product['year'],$attrOptionValue)){
                                        $attrOptionValue[]=$product['year'];
                                    }
                                }
                            }
                            
                        }
                      /*  echo $optionId;exit;
                        $options['delete'][$optionId] = true; 
                        $options['value'][$optionId] = true;*/
                       
                    for($j=0;$j< count($attrOptionValue);$j++){
                        $attOptionValue=$attrOptionValue[$j];
                        $options['delete'][$attOptionValue] = true; 
                        $options['value'][$attOptionValue] = true;
                        
                    }
                    
                    $setup = new Mage_Eav_Model_Entity_Setup('core_setup');
                    $setup->addAttributeOption($options);
                    $write = Mage::getSingleton('core/resource')->getConnection('core_write');
                    if($name=="make"){
                    $sql  = "DELETE FROM filter WHERE ".$name."='".$optionId."'";
                    }
                    if($name=="model"){
                        $sql  = "UPDATE filter SET model='',year='',engineno='' WHERE ".$name."='".$optionId."'";
                    }
                    if($name=="engineno"){
                        $sql  = "UPDATE filter SET year='',engineno=''  WHERE ".$name."='".$optionId."'";
                    }
                    if($name=="year"){
                        
                        $sql  = "UPDATE filter SET year='' WHERE ".$name."='".$optionId."'";
                    }
                    $write->query($sql); 
                    
                    $data_array=array();
                    //For fill combo box
                    if($name == "make"){
                        $attribute = Mage::getSingleton('eav/config')->getAttribute('catalog_product',$name);
                        if ($attribute->usesSource()) {
                                $options = $attribute->getSource()->getAllOptions(false);
                                $cnt=0;
                                if($name=='engineno'){
                                    $data_array[''] = "Select Engine";
                                }else{
                                    $data_array[''] = "Select ".ucfirst($name);
                                }
                                //$data_array['']='Select '.$name;
                                    for($j=0;$j<count($options);$j++){
                                            $makeoption=$options[$j]['value'];
                                            $data_array[$makeoption]=$options[$j]['label'];
                                    }
                                
                            }
                    } else {
                            if($name=='engineno'){
                                $data_array[''] = "Select Engine";
                            }else{
                                $data_array[''] = "Select ".ucfirst($name);
                            }
                            //$data_array[''] = "Select ".$name;
                                $val = "1=1";
                                if($name == "model"):
                                    $val = "make='".$make."'";
                                elseif($name == "engineno"):
                                    $val = "make='".$make."' && model='".$model."'";
                                elseif($name == "year"):
                                    $val = "make='".$make."' && model='".$model."' && engineno='".$engine."'";
                                endif;
                                $select ="select * from filter where $val";

                                $rowArray =$connection->fetchAll($select);
                                foreach ($rowArray as $val) {
                                    $filval=$val[$name];
                                        $attSelect ="select value from eav_attribute_option_value where option_id='".$filval."'";
                                    $filName=$connection->fetchRow($attSelect);
                                    if($filval!="" && $filval!="0"){
                                        $data_array[$filval] = $filName['value'];
                                    }

                                }
                    }
                        
                        
                    echo json_encode($data_array);
                     exit;
                     
                }
			
}
