<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */
class Mage_Catalog_Block_Product_List_Promotion extends Mage_Catalog_Block_Product_List
{
    protected function _getProductCollection()
    {
		if (is_null($this->_productCollection)) {
                    $coreSession = Mage::getSingleton("core/session");
                $session = Mage::getSingleton("customer/session");
		//print_r($this->getRequest()->getParams());
                $catId = $this->getRequest()->getParam('catId');
                    $coreSession->setData("sesCatId",$catId);
                
                $make = $this->getRequest()->getParam('make');
                
                if($make!=""){
                    $session->setData("sesMake",$make);
                    
                }else{ 
                    $make =$session->getData('sesMake');
                }
                
                $model = $this->getRequest()->getParam('model');
                if($model!=""){
                    $session->setData("sesModel",$model);
                    
                }else{ 
                    $model =$session->getData('sesModel');
                }
               
                $year = $this->getRequest()->getParam('year');
                if($year!=""){
                    $session->setData("sesYear",$year);
                    
                }else{ 
                    $year =$session->getData('sesYear');
                }
                 
                $engineno=$this->getRequest()->getParam('engineno');
                if($engineno!=""){
                    $session->setData("sesEngineno",$engineno);
                    
                }else{ 
                    $engineno =$session->getData('sesEngineno');
                }
                
                
            $collection = Mage::getResourceModel('catalog/product_collection');
            Mage::getModel('catalog/layer')->prepareProductCollection($collection);
            $argArr = array('make','model','year','engineno');
            
            $attribute = Mage::getSingleton('eav/config')->getAttribute('catalog_product', 'make');
        if ($attribute->usesSource()) {
            $options = $attribute->getSource()->getAllOptions(false);
            for($j=0;$j<count($options);$j++){ 
                if($options[$j]['label']==$make){ 
                    $collection->addAttributeToFilter('make', $options[$j]['value'])->addStoreFilter();
                }
                
            }
        }
        $attribute = Mage::getSingleton('eav/config')->getAttribute('catalog_product', 'model');
        if ($attribute->usesSource()) {
            $options = $attribute->getSource()->getAllOptions(false);
            for($j=0;$j<count($options);$j++){ 
                if($options[$j]['label']==$model){ 
                    $collection->addAttributeToFilter('model', $options[$j]['value'])->addStoreFilter();
                }
                
            }
        }
        $attribute = Mage::getSingleton('eav/config')->getAttribute('catalog_product', 'engineno');
        if ($attribute->usesSource()) {
            $options = $attribute->getSource()->getAllOptions(false);
            for($j=0;$j<count($options);$j++){ 
                if($options[$j]['label']==$engineno){ 
                    $collection->addAttributeToFilter('engineno', $options[$j]['value'])->addStoreFilter();
                }
                
            }
        }
        $attribute = Mage::getSingleton('eav/config')->getAttribute('catalog_product', 'year');
        if ($attribute->usesSource()) {
            $options = $attribute->getSource()->getAllOptions(false);
            for($j=0;$j<count($options);$j++){ 
                if($options[$j]['label']==$year){ 
                    $collection->addAttributeToFilter('year', $options[$j]['value'])->addStoreFilter();
                }
                
            }
        }
        
                //$parentCategoryId = $catId;
                
                /*$category = Mage::getModel('catalog/category')
                ->getCollection()
                ->addFieldToFilter('parent_id', array('eq'=>$parentCategoryId))
                ->addFieldToFilter('is_active', array('eq'=>'1'))
                ->addAttributeToSelect('*');*/
                //echo "here<pre>";print_r($category);exit;
            //$category = Mage::registry('current_category'); 
            if($catId!=""){ 
                $category1 = Mage::getModel('catalog/category')->load($catId); 
               
                $collection->addCategoryFilter($category1);
            }
 // $collection->getSelect();

            $this->_productCollection = $collection;
        }
        
        return $this->_productCollection;
    }
}
