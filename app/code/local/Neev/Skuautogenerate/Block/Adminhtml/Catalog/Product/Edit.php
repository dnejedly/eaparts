<?php
/**
 * Magento Enterprise Edition
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Magento Enterprise Edition License
 * that is bundled with this package in the file LICENSE_EE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://www.magentocommerce.com/license/enterprise-edition
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2011 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://www.magentocommerce.com/license/enterprise-edition
 */

/**
 * Customer edit block
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */

class Neev_Skuautogenerate_Block_Adminhtml_Catalog_Product_Edit extends Mage_Adminhtml_Block_Catalog_Product_Edit
{
public function __construct()
    {
        parent::__construct();
            $this->setTemplate('skuautogenerate/catalog/product/edit.phtml');
        $this->setId('product_edit');
    }
   

    public function skuAuto($type)
    { 
      $connection = Mage::getSingleton('core/resource')->getConnection('read');
      $table = Mage::getSingleton('core/resource')->getTableName('skuautogenerate');
      $skuauto = $connection->query("SELECT * FROM {$table} WHERE status = 1 AND product_type='{$type}'");
      $skuautores=$skuauto->fetch(); 
      if(!$skuautores["skuautogenerate_id"]){
       $skuauto = $connection->query("SELECT * FROM {$table} WHERE product_type='unique'");
       $skuautores = $skuauto->fetch();}
       return $skuautores;         

   }

public function getprevEntityId()
    { 
      $connection = Mage::getSingleton('core/resource')->getConnection('read');
      $table = Mage::getSingleton('core/resource')->getTableName('catalog_product_entity');
       $sql = "SELECT entity_id FROM {$table} ORDER BY entity_id DESC LIMIT 1";
      $getprevEntityId=$connection->fetchOne($sql); 
       return $getprevEntityId;         
       
   }
   
}
