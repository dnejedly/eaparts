function showhide(id){
        
        var make = document.getElementById(id);
        make.style.display = "block";
        
}
   

function getModelList(val,model){ 
        var reloadurl = urlmodel;
        var param=  'make='+val+'&model=0&engineno=0';
        if(val!='0'){
	new Ajax.Request(reloadurl, {
	method: 'post',
	parameters: param,
	onComplete: function(transport) {
            $('model').innerHTML = "";
            response = transport.responseText;
            var select = document.getElementById('model');
            select.options[select.options.length] =  new Option('Select Model',0);
            var obj = JSON.parse(response);
            //alert(obj.toSource())
            var combobox = "";
            var ob;
            if(obj != ""){
				for(ob in obj){
				if(ob==''){
					// $('year').innerHTML = '<option value="0">Select Year</option>';
					// $('engineno').innerHTML = '<option value="0">Select Engine</option>';
				}    
					select.options[select.options.length] =  new Option(obj[ob],ob);
				}
				// For Show Selected Value
							var options = $('model');
							var len = options.length;
							for (var i = 0; i < len; i++) {
								//console.log('Option text = ' + options[i].text);
								//console.log('Option value = ' + options[i].value);
								if(options[i].value == model){
									options[i].selected = true;
								}
							}
							// End Show Selected Value
			  }
             if(model != ''){
                 engineno=document.getElementById('engineno').value;                 
                getEngnoList(model,engineno);
             }
         }
	});
        }
}
function getEngnoList(val,engineno){ 
        var reloadurl = urlengine;
        
	var param=  'make='+$('make').value+'&model='+val+'&engineno=0';
       if(val!='0'){
	new Ajax.Request(reloadurl, {
	method: 'post',
	parameters: param,
	onComplete: function(transport) {
            $('engineno').innerHTML = "";
            response = transport.responseText;
            var select = document.getElementById('engineno');
            select.options[select.options.length] =  new Option('Select Engine',0);
            var obj = JSON.parse(response);
            var ob;
            //alert(obj.toSource())
            if(obj != ""){
				for(ob in obj){
				if(ob==''){
				  //  $('year').innerHTML = '<option value="0">Select Year</option>';
				}
				select.options[select.options.length] =  new Option(obj[ob],ob);    

				}
				// For Show Selected Value
				var options = $('engineno');
				var len = options.length;
				for (var i = 0; i < len; i++) {
					//console.log('Option text = ' + options[i].text);
					//console.log('Option value = ' + options[i].value);
					if(options[i].value == engineno){
						options[i].selected = true;
					}
				}
				// End Show Selected Value
			}
            if(engineno != ''){
                year=document.getElementById('year').value;                 
                getYearList(engineno,year);
            }
        }
	});}
}
function getYearList(val,year){ 

        var reloadurl =urlyear;
        
	var param=  'make='+$('make').value+'&model='+$('model').value+'&engineno='+val;
        if(val!='0'){
	new Ajax.Request(reloadurl, {
	method: 'post',
	parameters: param,
	onComplete: function(transport) {
            $('year').innerHTML = "";
            response = transport.responseText;
            var select = document.getElementById('year');
            select.options[select.options.length] =  new Option('Select Year',0);
            var obj = JSON.parse(response);
            var combobox = "";
            var ob;
            if(obj != ""){
				for(ob in obj){
					select.options[select.options.length] =  new Option(obj[ob],ob);    
									
				}
				// For Show Selected Value
				var options = $('year');
				var len = options.length;
				for (var i = 0; i < len; i++) {
					//console.log('Option text = ' + options[i].text);
					//console.log('Option value = ' + options[i].value);
					if(options[i].value == year){
						options[i].selected = true;
					}
				}
				// End Show Selected Value
			}
        }
	});
        }
}

// For Add Make
function showHideEdit(id){
    var make =  document.getElementById(id).value;
    if(make == 0){
        alert("Please select "+id);
        return false;
    }
    var index = document.getElementById(id).selectedIndex;
    document.getElementById(id+'-id').value = make;
    document.getElementById(id+'-text').value = document.getElementById(id).options[index].text;
    var divId = 'div'+id;
    var make = document.getElementById(divId);
    make.style.display = "block";
}
function addMake(name){
    var addValue = "";
    var parm = "";
    if(name == "model"){
        addValue = document.getElementById("make").value;
        parm = "&make="+addValue+"&model=0&engine=0&year=0";
        if(addValue == "" || addValue == 0){
            alert("Please Select Make");
            return false;
        }
    }
    if(name == "engineno"){
        addValue = document.getElementById("model").value;
        parm = "&make="+document.getElementById("make").value+"&model="+addValue+"&engine=0&year=0";
        if(addValue == "" || addValue == 0){
            alert("Please Select Model");
            return false;
        }
    }
    if(name == "year"){
        addValue = document.getElementById("engineno").value;
        parm = "&make="+document.getElementById("make").value+"&model="+document.getElementById("model").value+"&engine="+addValue+"&year=0";
        if(addValue == "" || addValue == 0){
            alert("Please Select Engine");
            return false;
        }
    }
    var fieldText = name+'-text';
    var fieldId = name+'-id';
    var fieldTextVal = document.getElementById(fieldText).value;
    var fieldIdVal = document.getElementById(fieldId).value;
    if(fieldTextVal == ""){
        alert("Please enter "+name);
        return false;
    }
    //alert(fieldTextVal);
    //alert(fieldIdVal);
		
       var reloadurl = urladdoption;
       var param=  'fieldTextVal='+fieldTextVal+'&fieldIdVal='+fieldIdVal+'&attName='+name+'&attValue='+addValue+parm;
	new Ajax.Request(reloadurl, {
	method: 'post',
	parameters: param,
	onComplete: function(transport) {
                response = transport.responseText;
                if(response == 1){
                    alert("Already Exists.")
                } else {
					$(name).innerHTML = "";
                    var select = document.getElementById(name);
                    if(name=='engineno'){
						select.options[select.options.length] =  new Option('Select Engine',0);
					} else {
						select.options[select.options.length] =  new Option('Select '+ucfirst(name),0);
					}
                    var obj = JSON.parse(response);
                    var combobox = "";
                    var ob;
                    
                    for(ob in obj){
							select.options[select.options.length] =  new Option(obj[ob],ob); 
                    }
                    //$(name).innerHTML = combobox;
                }
            }
        });
    document.getElementById(fieldText).value = "";
    document.getElementById(fieldId).value = 0; 
    document.getElementById("div"+name).style.display = "none";
}
// End Add Make
// For Delete Attribute
function attDelete(name){
    var nameVal =  document.getElementById(name).value;
    var make = document.getElementById("make").value;
    var model = document.getElementById("model").value;
    var engineno = document.getElementById("engineno").value;
    var year = document.getElementById("year").value;
    if(nameVal == 0){
         if(name=='engineno'){
                        name='Engine';
                    }
        alert("Please select "+ucfirst(name));
         if(name=='Engine'){
                        name='engineno';
                    }
        return false;
    }
    var r=confirm("Are you sure you want to delete(s)?");
    if (r==true)
    {
        var parm = "";
        if(name == "model"){
            parm = "&make="+document.getElementById("make").value+"&model=0&engine=0&year=0";
        }
        if(name == "engineno"){
            parm = "&make="+document.getElementById("make").value+"&model="+document.getElementById("model").value+"&engine=0&year=0";
        }
        if(name == "year"){
            parm = "&make="+document.getElementById("make").value+"&model="+document.getElementById("model").value+"&engine="+document.getElementById("engineno").value+"&year=0";
        }
        //alert(nameVal);
        var reloadurl = urldeleteoption;
            var param=  'optionId='+nameVal+'&name='+name+parm;
            new Ajax.Request(reloadurl, {
            method: 'post',
            parameters: param,
            onComplete: function(transport) {
                    response = transport.responseText;
                    //  alert(response);
                    if(response == 1){
                        alert("Already Exists.")
                    } else {
                        $(name).innerHTML = "";
                        var select = document.getElementById(name);
						select.options[select.options.length] =  new Option('Select '+name,0);
                        var obj = JSON.parse(response);
                        var combobox = "";
                        var ob;
                        //combobox = '<option value="0">Select '+name+'</option>'
                        for(ob in obj){
                               	select.options[select.options.length] =  new Option(obj[ob],ob);   
                        }
                        //$(name).innerHTML = combobox;
                        if(name == "make"){
                            getModelList(make,"");
                            getEngnoList(model,"");
                            getYearList(engineno,"");
                        }
                        if(name == "model"){
							getEngnoList(model,"");
                            getYearList(engineno,"");
                        }
                        if(name == "engineno"){
                            getYearList(engineno,"");
                        }

                    }
                }
            });
      }
    /*document.getElementById(fieldText).value = "";
    document.getElementById(fieldId).value = 0; 
    document.getElementById("div"+name).style.display = "none";*/
    
}
// End Delete Attribute

function ucfirst(string){
	    return string.charAt(0).toUpperCase() + string.slice(1);
}
